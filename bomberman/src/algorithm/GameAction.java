/* ******************************************************
 * Simulator alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: algorithms/RandomWalker.java 2015-03-09 buixuan.
 * ******************************************************/
package algorithm;

import java.util.ArrayList;

import data.Position;
import gestionson.Sound;
import javafx.geometry.NodeOrientation;
import objets.Bombe;
import specifications.AlgorithmService;
import specifications.DataService;
import specifications.SimulatorService;
import tools.HardCodedParameters;
import specifications.RequireSimulatorService;

public class GameAction implements AlgorithmService, RequireSimulatorService{
  private SimulatorService simulator;
  private DataService data;

  public GameAction() {}

  @Override
  public void bindSimulatorService(SimulatorService service){
    simulator = service;
  }
  
  @Override
  public void bindDataService(DataService service){
    data=service;
  }
  
  @Override
  public void activation(){
    simulator.moveRight();
  }
  
  @Override
  public void stepAction(){
	  
	  ArrayList<Bombe> list_pos = (ArrayList<Bombe>) data.getList_bombe().clone();
	  try {
		data.getAccess_bombe().acquire();
	  } catch (InterruptedException e) {
		e.printStackTrace();
	  };
	  for (Bombe bombe : list_pos) {
		  if (bombe.getTime_before_explode() == 0) {
	  		data.setSoundEffect(Sound.Bombe);
			data.deleteBombe(bombe);
		  }else
			 bombe.setTime_before_explode(bombe.getTime_before_explode()-1);
  	  }
	  data.getAccess_bombe().release();
	  data.getPlayer().isHit();

	  if (data.getGoLeft()) simulator.moveLeft();
	  if (data.getGoRight()) simulator.moveRight();
	  if (data.getGoUp()) simulator.moveUp();
	  if (data.getGoDown()) simulator.moveDown();
	  int ret = 0;
	  while (ret !=-1) {
		  
		  ret = calculNewPositionGhostX(ret);
		  ret = calculNewPositionGhostY(ret);
	  }
	  

  }
  
  
  

	private int calculNewPositionGhostX(int cpt) {
		
		if (cpt == -1 || data.getGhost().size() == cpt)
			return -1;
		
		if (data.getGhost().get(cpt).coeur == 0) {
			return cpt+1;
		}
			
		
		
		data.getGhost().get(cpt).isHit();
		
		Position target = data.getHeroesPosition();
		Position self = data.getGhost().get(cpt).hitboxGhost.center;
		Position selftopl = data.getGhost().get(cpt).hitboxGhost.top_left;
		
		
		if (self.x + HardCodedParameters.size_hitbox_body_width/2 < target.x) {
			self.x += data.getSpeed_ghost();
			selftopl.x += data.getSpeed_ghost();
			data.getGhost().get(cpt).orientation = NodeOrientation.RIGHT_TO_LEFT;
			
			return cpt+1;
		}
		if (self.x - HardCodedParameters.size_hitbox_body_width/2 > target.x) {
			self.x -= data.getSpeed_ghost();
			selftopl.x  -= data.getSpeed_ghost();
			data.getGhost().get(cpt).orientation = NodeOrientation.LEFT_TO_RIGHT;
			return cpt+1;
		}
		if (self.y + HardCodedParameters.size_hitbox_body_width/2 < target.y) {
			self.y += data.getSpeed_ghost();
			selftopl.y += data.getSpeed_ghost();
			return cpt+1;
		}
		if (self.y - HardCodedParameters.size_hitbox_body_width/2 > target.y) {
			self.y -= data.getSpeed_ghost();
			selftopl.y -= data.getSpeed_ghost();
			return cpt+1;
		}
		return cpt+1;
	}
	
	private int calculNewPositionGhostY(int cpt) {
		
		if (cpt == -1 || data.getGhost().size() == cpt)
			return -1;
		
		if (data.getGhost().get(cpt).coeur == 0) {
			return cpt+1;
		}
			
		
		
		data.getGhost().get(cpt).isHit();
		
		Position target = data.getHeroesPosition();
		Position self = data.getGhost().get(cpt).hitboxGhost.center;
		Position selftopl = data.getGhost().get(cpt).hitboxGhost.top_left;
		
		
		
		if (self.y + HardCodedParameters.size_hitbox_body_width/2 < target.y) {
			self.y += data.getSpeed_ghost();
			selftopl.y += data.getSpeed_ghost();
			return cpt+1;
		}
		if (self.y - HardCodedParameters.size_hitbox_body_width/2 > target.y) {
			self.y -= data.getSpeed_ghost();
			selftopl.y -= data.getSpeed_ghost();
			return cpt+1;
		}
		
		if (self.x + HardCodedParameters.size_hitbox_body_width/2 < target.x) {
			self.x += data.getSpeed_ghost();
			selftopl.x += data.getSpeed_ghost();
			data.getGhost().get(cpt).orientation = NodeOrientation.RIGHT_TO_LEFT;
			
			return cpt+1;
		}
		if (self.x - HardCodedParameters.size_hitbox_body_width/2 > target.x) {
			self.x -= data.getSpeed_ghost();
			selftopl.x  -= data.getSpeed_ghost();
			data.getGhost().get(cpt).orientation = NodeOrientation.LEFT_TO_RIGHT;
			return cpt+1;
		}
		return cpt+1;
	}
}
