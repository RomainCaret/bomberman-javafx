package data;

import tools.HardCodedParameters;

public class Explosion {
    private boolean[][] map;
    private int hit_frames;
    public Explosion(boolean[][] map){
        this.map = map;
        hit_frames = HardCodedParameters.hit_bombe_frames;
    }

    public boolean[][] get_map(){
        return this.map;
    }
    public void decrease_hit_frames(){
        this.hit_frames--;
    }
    public int get_hit_frames(){
        return hit_frames;
    }

    
}
