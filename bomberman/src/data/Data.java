/* ******************************************************
 * Simulator alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: data/Data.java 2015-03-09 buixuan.
 * ******************************************************/
package data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;

import bodies.Ghost;
import bodies.Player;
import gestionson.SonJeu;
import gestionson.Sound;
import gif.Animation;
import javafx.geometry.NodeOrientation;
import javafx.scene.image.ImageView;
import objets.Bombe;
import objets.Crate;
import objets.Objets;
import objets.Wall;
import specifications.DataService;
import tools.HardCodedParameters;

public class Data implements DataService {
	// private Heroes hercules;
	private Player player;
	private int stepNumber;
	private NodeOrientation orientation;
	private boolean goLeft;
	private boolean goRight;
	private boolean goUp;
	private boolean goDown;
	private ArrayList<Bombe> list_bombe;
	private ArrayList<Wall> list_wall_unsafe;
	private List<Wall> list_wall;
	private ArrayList<Trash> to_delete;
	private Semaphore access_bombe;
	private int taille_explo;
	private int map_wall_crate[][];
	private double speed_player;
	private double speed_ghost;


	private int map_level_1[][] = new int[13][13];
	private int map_level_2[][] = new int[13][13];
	private int map_level_3[][] = new int[13][13];

	private double big_birdX, birdsX;
	private NodeOrientation orientation_bird;
	private ArrayList<Explosion> map_hit;
	private Sound sound;
	private ArrayList<Sound> soundQueu= new ArrayList<Sound>();

	private ArrayList<Ghost> ghosts_unsafe;
	private List<Ghost> ghosts;
	private int nb_bombe_max;
	
	private int levelplayer;
	
	private SonJeu b;

	public Data() {
		
		//0 : Rien
		//1 : Mur
		//2 : Crate vide
		//3 : Crate with key
		//4 : Crate with speed bonus
		//5 : Crate with heart
		//6 : Crate with Explosive range +
		//7 : Crate with Explosive +1
		//9 : Door exit
		int map1[][] = { 
				{ 0, 2, 4, 2, 2, 7, 0, 0, 2, 0, 2, 0, 2 }, 
				{ 0, 1, 0, 1, 2, 1, 2, 1, 0, 1, 2, 1, 0 },
				{ 0, 0, 0, 0, 6, 2, 0, 0, 2, 0, 0, 2, 2 }, 
				{ 2, 1, 0, 1, 0, 1, 0, 1, 7, 1, 0, 1, 0 },
				{ 0, 2, 2, 7, 2, 0, 0, 4, 0, 0, 0, 0, 0 }, 
				{ 2, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2 },
				{ 2, 0, 2, 0, 2, 2, 2, 0, 2, 0, 0, 0, 4 }, 
				{ 0, 1, 0, 1, 2, 1, 5, 1, 0, 1, 0, 1, 2 },
				{ 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0 }, 
				{ 2, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0 },
				{ 2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, 
				{ 6, 1, 2, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2 },
				{ 3, 0, 2, 0, 0, 2, 0, 0, 0, 0, 2, 0, 9 } };
		for (int i = 0; i <  map1.length; i++) {
			for (int j = 0; j <  map1[0].length; j++) {
				map_level_1[i][j] = map1[j][i];
			}
		}

		int map2[][] = 
					{{0,2,9,0,2,2,0,0,2,0,2,0,2}, 
					{0,1,2,1,2,1,2,1,0,1,2,1,0},
					{0,0,2,0,2,2,0,0,2,2,0,2,2}, 
					{2,1,0,1,0,1,0,1,0,1,0,1,0},
					{4,0,2,2,2,0,0,2,0,2,2,2,2}, 
					{2,1,0,1,0,1,0,1,0,1,0,1,2},
					{0,0,2,0,2,2,0,2,2,2,0,0,2}, 
					{0,1,0,1,0,1,0,1,0,1,0,1,2},
					{0,0,2,0,0,2,2,2,0,2,2,2,0}, 
					{0,1,0,1,2,1,0,1,2,1,0,1,0},
					{0,0,0,2,2,0,2,2,0,2,2,2,2}, 
					{0,1,2,1,0,1,0,1,2,1,2,1,0},
					{2,0,2,0,0,2,0,0,2,0,2,4,3}};

		for (int i = 0; i <  map2.length; i++) {
			for (int j = 0; j <  map2[0].length; j++) {
				map_level_2[i][j] = map2[j][i];
			}
		}

		int map3[][] = { 
				{ 0, 0, 0, 5, 0, 0, 2, 0, 0, 0, 0, 0, 2 }, 
				{ 0, 1, 2, 1, 2, 1, 2, 1, 2, 1, 4, 1, 0 },
				{ 0, 0, 2, 0, 2, 2, 0, 0, 2, 0, 2, 3, 4 }, 
				{ 4, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0 },
				{ 0, 5, 2, 7, 0, 0, 0, 2, 0, 0, 0, 0, 0 }, 
				{ 2, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2 },
				{ 2, 0, 2, 0, 2, 2, 0, 0, 2, 0, 0, 0, 2 }, 
				{ 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2 },
				{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0 }, 
				{ 2, 1, 0, 1, 0, 1, 0, 1, 2, 1, 0, 1, 0 },
				{ 2, 0, 0, 2, 0, 0, 0, 0, 2, 0, 2, 0, 0 }, 
				{ 2, 1, 2, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0 },
				{ 2, 0, 2, 0, 0, 2, 0, 2, 2, 0, 2, 0, 9 } };
		for (int i = 0; i <  map3.length; i++) {
			for (int j = 0; j <  map3[0].length; j++) {
				map_level_3[i][j] = map3[j][i];
			}
		}
		this.access_bombe = new Semaphore(1);
		this.stepNumber = 0;
		this.birdsX = 1370;
		this.big_birdX = 1220;
		this.orientation_bird = NodeOrientation.LEFT_TO_RIGHT;
		this.map_wall_crate = new int[13][13];
		this.speed_player = HardCodedParameters.speed_init;
		this.taille_explo = HardCodedParameters.taille_explosion_init;
		this.nb_bombe_max = HardCodedParameters.nb_bombe_max_init;
		this.ghosts_unsafe = new ArrayList<Ghost>();
		
		this.ghosts = Collections.synchronizedList(this.ghosts_unsafe);
		this.list_wall_unsafe = new ArrayList<Wall>();
		this.list_wall = Collections.synchronizedList(this.list_wall_unsafe);
		
	}
	@Override
	public double getSpeed_player() {
		return speed_player;
	}

	@Override
	public void speed_up_player() {
		this.speed_player = this.speed_player +0.1;
	}
	
	@Override
	public void explosive_up() {
		this.nb_bombe_max++;
	}

	public void set_level_1() {
		
		this.levelplayer = 1;
		//Init de la map, chargement de la carte level 1
		for (int i = 0; i <  map_level_1.length; i++) {
			for (int j = 0; j <  map_level_1.length; j++) {
				this.map_wall_crate[i][j] = map_level_1[i][j];
			}
		}
		
		this.list_wall.clear();

		for (int i = 0; i <  this.map_wall_crate.length; i++) {
			for (int j = 0; j <  this.map_wall_crate[0].length; j++) {
				if ( this.map_wall_crate[i][j] == 1)
					list_wall.add(new Wall(false,
							new Position(
									HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left,
									HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up),
								HardCodedParameters.size_square, HardCodedParameters.size_square));

				else 
					if ( this.map_wall_crate[i][j] != 0 && this.map_wall_crate[i][j] != 9)
					list_wall.add(new Crate(
							new Position(
									HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left,
									HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up),
								HardCodedParameters.size_square, HardCodedParameters.size_square, i, j));
			}
		}
		this.player = new Player(new Position(470, 140),this,3);
		this.stepNumber = 0;
		this.orientation = NodeOrientation.LEFT_TO_RIGHT;
		this.goLeft = false;
		this.goUp = false;
		this.goDown = false;
		this.goRight = false;
		this.list_bombe = new ArrayList<Bombe>();
		this.to_delete = new ArrayList<Trash>();
		this.sound = Sound.None;
		this.access_bombe = new Semaphore(1);
		this.map_hit = new ArrayList<Explosion>();
		this.ghosts.clear();
		this.ghosts.add(new Ghost(new Position(1053,721), 1, this));
		this.access_bombe = new Semaphore(1);
		this.speed_ghost = HardCodedParameters.speedGhostInit;
		
	}
	public void set_level_2(boolean reset) {
		if (reset == false)
			b.start_sound_boucle("src/sound/mario.wav",100, 0.1, false, Sound.MainSong); //String file, délai, skip premiere fois, mainSong
		this.levelplayer = 2;
		//Init de la map, chargement de la carte level 1
		for (int i = 0; i <  map_level_2.length; i++) {
			for (int j = 0; j <  map_level_2.length; j++) {
				this.map_wall_crate[i][j] = map_level_2[i][j];
			}
		}
		this.list_wall.clear();

		for (int i = 0; i <  this.map_wall_crate.length; i++) {
			for (int j = 0; j <  this.map_wall_crate[0].length; j++) {
				if ( this.map_wall_crate[i][j] == 1)
					list_wall.add(new Wall(false,
							new Position(
									HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left,
									HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up),
								HardCodedParameters.size_square, HardCodedParameters.size_square));

				else 
					if ( this.map_wall_crate[i][j] != 0 && this.map_wall_crate[i][j] != 9)
					list_wall.add(new Crate(
							new Position(
									HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left,
									HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up),
								HardCodedParameters.size_square, HardCodedParameters.size_square, i, j));
			}
		}
		this.player = new Player(new Position(470, 140),this, this.player.coeur == 0 ? 3 : this.player.coeur);
		this.stepNumber = 0;
		this.orientation = NodeOrientation.LEFT_TO_RIGHT;
		this.goLeft = false;
		this.goUp = false;
		this.goDown = false;
		this.goRight = false;
		this.list_bombe = new ArrayList<Bombe>();
		this.to_delete = new ArrayList<Trash>();
		this.sound = Sound.None;
		this.access_bombe = new Semaphore(1);
		this.map_hit = new ArrayList<Explosion>();
		this.ghosts.clear();
		this.ghosts.add(new Ghost(new Position(1053,721), 1, this));
		this.ghosts.add(new Ghost(new Position(1040,733), 1, this));
		this.access_bombe = new Semaphore(1);
		this.speed_ghost = 0.7;
	}

	public void set_level_3(boolean reset) {
		if (reset == false) {
			b.start_sound_boucle("src/sound/godsandglory.wav",100, 0.1, false, Sound.MainSong); //String file, délai, skip premiere fois, mainSong
		}
		this.levelplayer = 3;
		//Init de la map, chargement de la carte level 1
		for (int i = 0; i <  map_level_3.length; i++) {
			for (int j = 0; j <  map_level_3.length; j++) {
				this.map_wall_crate[i][j] = map_level_3[i][j];
			}
		}
		this.list_wall.clear();

		for (int i = 0; i <  this.map_wall_crate.length; i++) {
			for (int j = 0; j <  this.map_wall_crate[0].length; j++) {
				if ( this.map_wall_crate[i][j] == 1)
					list_wall.add(new Wall(false,
							new Position(
									HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left,
									HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up),
								HardCodedParameters.size_square, HardCodedParameters.size_square));

				else 
					if ( this.map_wall_crate[i][j] != 0 && this.map_wall_crate[i][j] != 9)
					list_wall.add(new Crate(
							new Position(
									HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left,
									HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up),
								HardCodedParameters.size_square, HardCodedParameters.size_square, i, j));
			}
		}
		this.player = new Player(new Position(470, 140),this, this.player.coeur == 0 ? 3 : this.player.coeur);
		this.stepNumber = 0;
		this.orientation = NodeOrientation.LEFT_TO_RIGHT;
		this.goLeft = false;
		this.goUp = false;
		this.goDown = false;
		this.goRight = false;
		this.list_bombe = new ArrayList<Bombe>();
		this.to_delete = new ArrayList<Trash>();
		this.sound = Sound.None;
		this.access_bombe = new Semaphore(1);
		this.map_hit = new ArrayList<Explosion>();
		this.ghosts.clear();
		if (reset == false)
			this.big_birdX -= 300;
		this.ghosts.add(new Ghost(new Position(1053,721),2, this));
		this.ghosts.add(new Ghost(new Position(1040,733),2, this));
		this.access_bombe = new Semaphore(1);
		this.speed_ghost = 0.95;
	}
	@Override
	public double getSpeed_ghost() {
		return speed_ghost;
	}
	@Override
	public void son_end() {
		b.start_sound_boucle("src/sound/end.wav",100, 0.5, false, Sound.MainSong);
	}
	
	@Override
	public void bind_bruit_boucle(SonJeu b) {
		this.b = b;
	}
	/**
	 * @return the levelplayer
	 */
	@Override
	public int getLevelplayer() {
		return levelplayer;
	}

	/**
	 * @param levelplayer the levelplayer to set
	 */
	@Override
	public void setLevelplayer(int levelplayer) {
		this.levelplayer = levelplayer;
	}

	@Override
	public List<Ghost> getGhost() {
		return ghosts;
	}
	public Sound getSoundEffect() {
		return sound;
	}

	public Sound setSoundEffect(Sound s) {
		if (s == Sound.None && soundQueu.size() >0) {
			sound = soundQueu.get(0);
			soundQueu.remove(0);
			return s;
		}
		return sound = s;
	}

	public double getBig_birdX() {
		return big_birdX;
	}

	public double getBirdsX() {
		return birdsX;
	}
	@Override
	public void stepBird(boolean dragon) {
		if (dragon) {
			if (orientation_bird == NodeOrientation.RIGHT_TO_LEFT) {
				big_birdX = big_birdX - 100;		
			}else {
				big_birdX = big_birdX + 100;	
			}
		}
		if (big_birdX < -1000) {
			orientation_bird = NodeOrientation.LEFT_TO_RIGHT;
		}
		if (big_birdX > 2000) {
			orientation_bird = NodeOrientation.RIGHT_TO_LEFT;
		}
		if (orientation_bird == NodeOrientation.RIGHT_TO_LEFT) {
			if (big_birdX == 800 && this.levelplayer !=3)
				setSoundEffect(Sound.Birds);
			if (big_birdX == 800 && this.levelplayer ==3)
				setSoundEffect(Sound.Dragon);
			big_birdX = big_birdX - 1;
			birdsX = birdsX - 1;		
		}else {
			if (big_birdX == 100 && this.levelplayer !=3)
				setSoundEffect(Sound.Birds);
			if (big_birdX == -50 && this.levelplayer ==3)
				setSoundEffect(Sound.Dragon);
			big_birdX = big_birdX + 1;
			birdsX = birdsX + 1;	
		}
	}

	public NodeOrientation getBirdOrientation() {
		return orientation_bird;
	}

	public ArrayList<Explosion> getMapHit(){
		return map_hit;
	}

	public int[][] getMap_wall_crate() {
		return map_wall_crate;
	}

	@Override
	public int getTaille_explo() {
		return taille_explo;
	}
	@Override
	public void upTaille_explo() {
		this.taille_explo = this.taille_explo+1;
	}

	public ArrayList<Trash> getTo_delete() {
		return this.to_delete;
	}

	public void clear_delete() {
		this.to_delete.clear();
	}

	public void add_delete(ImageView to_delete, int i, int j) {
		this.to_delete.add(new Trash(to_delete, i, j));
	}

	@Override
	public Semaphore getAccess_bombe() {
		return access_bombe;
	}

	@Override
	public void setAccess_bombe(Semaphore access_bombe) {
		this.access_bombe = access_bombe;
	}

	@Override
	public List<Wall> getList_wall() {
		return list_wall;
	}

	@Override
	public void setList_wall(ArrayList<Wall> list_wall) {
		this.list_wall = list_wall;
	}

	@Override
	public boolean getGoRight() {
		return goRight;
	}

	@Override
	public void setGoRight(boolean goRight) {
		this.goRight = goRight;
	}

	@Override
	public boolean getGoUp() {
		return goUp;
	}

	@Override
	public void setGoUp(boolean goUp) {
		this.goUp = goUp;
	}

	@Override
	public boolean getGoDown() {
		return goDown;
	}

	@Override
	public void setGoDown(boolean goDown) {
		this.goDown = goDown;
	}

	@Override
	public Position getHeroesPosition() {
		return player.hitboxPlayer.center;
	}

	@Override
	public Player getPlayer() {
		return player;
	}

	@Override
	public int getStepNumber() {
		return stepNumber;
	}

	@Override
	public void setHeroesPosition(Position p) {
		player.hitboxPlayer.setPosition(p);
	}

	@Override
	public void setStepNumber(int n) {
		stepNumber = n;
	}

	@Override
	public NodeOrientation getOrientation() {
		return orientation;
	}

	@Override
	public void setOrientation(NodeOrientation orientation) {
		this.orientation = orientation;
	}

	@Override
	public boolean getGoLeft() {
		return goLeft;
	}

	@Override
	public void setGoLeft(boolean goLeft) {
		this.goLeft = goLeft;
	}

	public ArrayList<Bombe> getList_bombe() {
		return list_bombe;
	}

	@Override
	public void addBomb() {
		int i = Math.round(Math.round((player.hitboxPlayer.top_left.x-15 - HardCodedParameters.limit_map_border_left)
				/ HardCodedParameters.size_square));
		int j = Math.round(Math.round((player.hitboxPlayer.top_left.y-10 - HardCodedParameters.limit_map_border_up)
				/ HardCodedParameters.size_square));

		if (list_bombe.size() < this.nb_bombe_max)
			list_bombe.add(new Bombe(new Position(
					HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left - 5
					+ HardCodedParameters.size_square / 2,
					HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up
					+ HardCodedParameters.size_square / 2),
					i, j));

	}

	@Override
	public void deleteBombe(Bombe bombe) {
		boolean wall1, wall2, wall3, wall4;

		boolean[][] map_delete = new boolean[13][13];

		for (boolean[] bs : map_delete) {
			for (int index = 0; index < bs.length; index++) {
				bs[index] = false;
			}

		}

		int i = bombe.getI();
		int j = bombe.getJ();

		wall1 = false;
		wall2 = false;
		wall3 = false;
		wall4 = false;
		map_delete[i][j] = true;
		for (int radius = 1; radius <= getTaille_explo(); radius++) {
			if (i + radius < HardCodedParameters.map_size && map_wall_crate[i + radius][j] == 1)
				wall1 = true;
			if (j + radius < HardCodedParameters.map_size && map_wall_crate[i][j + radius] == 1)
				wall2 = true;
			if (i - radius >= 0 && map_wall_crate[i - radius][j] == 1)
				wall3 = true;
			if (j - radius >= 0 && map_wall_crate[i][j - radius] == 1)
				wall4 = true;

			if (wall1 == false && i + radius < HardCodedParameters.map_size && map_wall_crate[i + radius][j] != 1) {
				map_delete[i + radius][j] = true;
			}
			if (wall2 == false && j + radius < HardCodedParameters.map_size && map_wall_crate[i][j + radius] != 1) {
				map_delete[i][j + radius] = true;
			}

			if (wall3 == false && i - radius >= 0 && map_wall_crate[i - radius][j] != 1) {
				map_delete[i - radius][j] = true;
			}
			if (wall4 == false && j - radius >= 0 && map_wall_crate[i][j - radius] != 1) {
				map_delete[i][j - radius] = true;
			}
		}
		List<Wall> list_wall_cp = (List<Wall>) this.list_wall_unsafe.clone();
		for (Wall crate : list_wall_cp) {
			if (crate.isBreakable()) {
				Crate c = ((Crate) crate);
				if (map_delete[c.getI()][c.getJ()]) {
					add_delete(c.getCrate(), bombe.getI(), bombe.getJ());
					this.list_wall.remove(c);
				}

			}
		}
		this.map_hit.add(new Explosion(map_delete));
		add_delete(bombe.getBombeGif().getView(), bombe.getI(), bombe.getJ());
		list_bombe.remove(bombe);
	}
	@Override
	public void addSound(Sound sound) {
		this.soundQueu.add(sound);
	}
}
