/* ******************************************************
 * Simulator alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: data/Position.java 2015-03-09 buixuan.
 * ******************************************************/
package data;

public class Position {
  public double x,y;
  
  public Position(double x, double y){
    this.x=x;
    this.y=y;
  }

  public double distance_carre(Position p){
    return (p.x-this.x)*(p.x-this.x)+(p.y-this.y)*(p.y-this.y);
  }

  public double distance(Position p){
    return Math.sqrt((p.x-this.x)*(p.x-this.x)+(p.y-this.y)*(p.y-this.y));
  }
  
  public String toString() {
	  return "Position("+x+","+y+")";
  }
}
