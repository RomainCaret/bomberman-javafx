package data;

import javafx.scene.image.ImageView;

public class Trash {
	private int i;
	private int j;
	private ImageView to_delete;
	
	public Trash(ImageView del, int i, int j) {
		to_delete = del;
		this.i = i;
		this.j = j;
	}

	public int getI() {
		return i;
	}

	public int getJ() {
		return j;
	}

	public ImageView getTo_delete() {
		return to_delete;
	}
}
