/* ******************************************************
 * Simulator alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: engine/Engine.java 2015-03-09 buixuan.
 * ******************************************************/
package engine;
//partie moteur de jeu
import specifications.EngineService;
import specifications.DataService;
import specifications.RequireDataService;
import tools.HardCodedParameters;
import specifications.AlgorithmService;
import specifications.CollideServices;
import specifications.RequireAlgorithmService;
import specifications.RequireCollideServices;
import data.Position;

import java.util.Timer;
import java.util.TimerTask;

public class Engine implements EngineService, RequireDataService, RequireAlgorithmService, RequireCollideServices{
  private Timer engineClock;
  private DataService data;
  private AlgorithmService algorithm;
  private CollideServices gest_collide;

  public Engine(){}

  @Override
  public void bindDataService(DataService service){
    data=service;
  }
  
  @Override
  public void bindGestCollide(CollideServices gest_collide){
    this.gest_collide=gest_collide;
  }
  

  @Override
  public void bindAlgorithmService(AlgorithmService service){
    algorithm=service;
  }
  
  @Override
  public void init(){
    engineClock = new Timer();
  }

  @Override
  public void start(){
    algorithm.activation();
    
    engineClock.schedule(new TimerTask(){
      public void run() {
        //System.out.println("Game step #"+data.getStepNumber()+": checked.");
        algorithm.stepAction();
        data.stepBird(false);
        
        data.setStepNumber(data.getStepNumber()+1);
      }
    },0,HardCodedParameters.enginePaceMillis);
  }
  
  @Override
  public void stop() {
	  engineClock.cancel();
  }
  
  @Override
  public void ghostmove(Position p){
      data.setHeroesPosition(new Position(data.getHeroesPosition().x-data.getSpeed_player(),data.getHeroesPosition().y));
  }
 

  @Override
  public void moveLeft(){
    if (data.getHeroesPosition().x - HardCodedParameters.size_hitbox_body_width/2 > HardCodedParameters.limit_map_border_left 
    && gest_collide.check_collide_player(data.getHeroesPosition().x-data.getSpeed_player(),data.getHeroesPosition().y))
      data.setHeroesPosition(new Position(data.getHeroesPosition().x-data.getSpeed_player(),data.getHeroesPosition().y));
  }
  
  @Override
  public void moveRight(){
    if (data.getHeroesPosition().x + HardCodedParameters.size_hitbox_body_width/2 < HardCodedParameters.limit_map_border_right
    		 && gest_collide.check_collide_player(data.getHeroesPosition().x+data.getSpeed_player(),data.getHeroesPosition().y))
      data.setHeroesPosition(new Position(data.getHeroesPosition().x+data.getSpeed_player(),data.getHeroesPosition().y));
  }
  
  @Override
  public void moveUp(){
    if (data.getHeroesPosition().y - HardCodedParameters.size_hitbox_body_height/2 > HardCodedParameters.limit_map_border_up
    		 && gest_collide.check_collide_player(data.getHeroesPosition().x,data.getHeroesPosition().y-data.getSpeed_player()))
      data.setHeroesPosition(new Position(data.getHeroesPosition().x,data.getHeroesPosition().y-data.getSpeed_player()));
  }
  
  @Override
  public void moveDown(){
    if (data.getHeroesPosition().y + HardCodedParameters.size_hitbox_body_height/2 < HardCodedParameters.limit_map_border_down
    		 && gest_collide.check_collide_player(data.getHeroesPosition().x,data.getHeroesPosition().y+data.getSpeed_player()))
      data.setHeroesPosition(new Position(data.getHeroesPosition().x,data.getHeroesPosition().y+data.getSpeed_player()));
  }
}
