package userInterface;

import data.Data;
import javafx.animation.Animation;
import javafx.util.Duration;

public class WaterRunnable implements Runnable {

	
	private Animation a;
	private int delay;
	private boolean exitThread;
	private Thread me;
	
	public WaterRunnable(Animation a, int delay, Data d) {
		this.a = a;
		this.delay = delay;
		this.exitThread = false;
		this.me = new Thread(this);
		this.me.start();
	}
	
	public void run() {
		while (exitThread == false) {
			a.setCycleCount(1);
			a.play();
			a.setDelay(new Duration(0));
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		System.out.println("FIN");
	}
	
	public void stopThread() {
        exitThread = true;
    }
}