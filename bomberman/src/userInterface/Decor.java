package userInterface;

import java.util.ArrayList;
import java.util.List;

import data.Data;
import gif.AnimatedGif;
import gif.Animation;
import javafx.geometry.NodeOrientation;
import javafx.scene.Node;
import javafx.scene.effect.Shadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import objets.Crate;
import objets.Wall;
import tools.HardCodedParameters;

public class Decor {
	private Viewer v;
	
	private Animation[] list_gifs_run_avatar;
	private int num_run_avatar;
	
	private Animation[] list_gifs_idle_avatar;
	private int num_idle_avatar;
	
	private Animation[] list_gifs_eau;
	private int num_eau;
	
	private Animation[] list_gifs_feu;
	private int num_feu;
	
	private Animation[] list_gifs_panda;
	private int num_panda;
	
	private Animation[] list_gifs_ghost;
	private int num_ghost;
	
	private Animation[] list_gifs_red_ghost;
	private int num_red_ghost;
	
	private Animation[] list_gifs_birds;
	private int num_birds;
	
	private Animation[] list_gifs_big_bird;
	private int num_big_bird;
	
	private Animation[] list_gifs_egg;
	private int num_egg;
	
	private Animation[] list_gifs_explosion;
	private int num_explosion;
	private int num_id_tab;
	
	private Data d;

	private Animation[] list_gifs_dragon;
	private int num_dragon;

	private Animation a_door;
	
	private Animation a_end;
	
	public Decor(Viewer viewer, Data d) {
		this.d  = d;
		this.v = viewer;
		
		this.a_end = new AnimatedGif("file:src/video/end.gif", 20000);
		
		this.list_gifs_dragon = AnimatedGif.create_tab_gif("file:src/img/dragfly.gif", 1000,3);
		this.num_dragon = 0;
		
		this.list_gifs_run_avatar = AnimatedGif.create_tab_gif("file:src/img/romainrun.gif", 300,3);
		this.num_run_avatar = 0;
		
		this.list_gifs_idle_avatar = AnimatedGif.create_tab_gif("file:src/img/romain_idle.gif",1000,3);
		this.num_idle_avatar = 0;
		
		this.list_gifs_eau = AnimatedGif.create_tab_gif("file:src/img/eau.gif", 1500,20);
		this.num_eau = 0;
		
		this.list_gifs_feu = AnimatedGif.create_tab_gif("file:src/img/feu2.gif", 700,30);
		this.num_feu = 0;
		
		this.list_gifs_panda = AnimatedGif.create_tab_gif("file:src/img/panda.gif", 9000,10);
		this.num_panda = 0;
		
		this.list_gifs_ghost = AnimatedGif.create_tab_gif("file:src/img/ghostbomberman.gif", 1000,5);
		this.num_ghost = 0;
		
		this.list_gifs_red_ghost = AnimatedGif.create_tab_gif("file:src/img/ghostred.gif", 1000,5);
		this.num_red_ghost = 0;
		
		this.list_gifs_birds = AnimatedGif.create_tab_gif("file:src/img/oiseaux.gif", 500,10);
		this.num_birds = 0;
		
		this.list_gifs_big_bird = AnimatedGif.create_tab_gif("file:src/img/oiseau1.gif", 700,10);
		this.num_big_bird = 0;
		
		this.list_gifs_egg = AnimatedGif.create_tab_gif("file:src/img/dragonegg.gif", 3200,10);
		this.num_egg = 0;
		
		this.list_gifs_explosion = AnimatedGif.create_tab_gif("file:src/img/explosion.gif", 800,HardCodedParameters.nb_bombe_max*20);
		this.num_explosion = 0;
		this.num_id_tab = 0;
		
		this.a_door = new AnimatedGif("file:src/img/door.gif", 2300);
		
		for (int i = 0; i < list_gifs_explosion.length; i++) {
			this.list_gifs_explosion[i].getView().setScaleX(HardCodedParameters.size_image_explosion);
			this.list_gifs_explosion[i].getView().setScaleY(HardCodedParameters.size_image_explosion);
		}
	}
	public void reset() {
		this.num_dragon = 0;
		this.num_run_avatar = 0;
		this.num_idle_avatar = 0;
		this.num_eau = 0;
		this.num_feu = 0;
		this.num_panda = 0;
		this.num_ghost = 0;
		this.num_red_ghost = 0;
		this.num_birds = 0;
		this.num_big_bird = 0;
		this.num_egg = 0;
		this.num_explosion = 0;
		this.num_id_tab = 0;
		this.a_door = new AnimatedGif("file:src/img/door.gif", 1600);
		
	}

	public ArrayList<Node> createObjects(int[][] map) {
		ArrayList<Node> list_objects = new ArrayList<Node>();
		
		for (int i = 0; i <  map.length; i++) {
			for (int j = 0; j <  map[0].length; j++) {	
				if (map[i][j] == 3) list_objects.add(create_key(i, j));	
				if (map[i][j] == 4) list_objects.add(create_speed(i, j));
				if (map[i][j] == 5) list_objects.add(create_heart_bonus(i, j));
				if (map[i][j] == 6) list_objects.add(create_explosive_range_bonus(i, j));
				if (map[i][j] == 7) list_objects.add(create_explosive_bonus(i, j));
				if (map[i][j] == 9) list_objects.add(create_door_gif(i, j));
			}
		}
		
		return list_objects;
	}

	

	public ArrayList<Node> createImageCrate(List<Wall> crates){
		ArrayList<Node> list_crates = new ArrayList<Node>();
		
		for (Wall wall : crates) {
			if (wall.isBreakable()) {
				Image image_crate = new Image("file:src/img/caisse.png");

				ImageView crate = new ImageView(image_crate);

				((Crate) wall).setCrate(crate);
				crate.setX(wall.top_left.x - 7);
				crate.setY(wall.top_left.y - 3);
				list_crates.add(crate);
			}
		}
		
		return list_crates;
	}
	
	public ImageView create_eau(int i, int j, int temps_attente) {
		Animation a_eau = this.list_gifs_eau[this.num_eau];
		this.num_eau++;


		a_eau.setDelay(Duration.millis(this.num_eau*500));
		a_eau.setCycleCount(-1);
		ImageView eau = a_eau.getView();
		eau.setScaleX(0.25);
		eau.setScaleY(0.25);
		

		eau.setX(HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left - 85);
		eau.setY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up - 75);
	
		a_eau.play();
		return eau;
	}
	
	public ImageView create_feu(int i, int j) {
		Animation a_feu = this.list_gifs_feu[this.num_feu];

		this.num_feu++;

		a_feu.setCycleCount(javafx.animation.Animation.INDEFINITE);
		a_feu.play();

		ImageView feu = a_feu.getView();
		feu.setScaleX(0.25);
		feu.setScaleY(0.25);

		feu.setX(HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left - 85);
		feu.setY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up - 80);


		return feu;
	}
	
	public ImageView create_panda(int i, int j, int delay) {
		Animation a_panda = this.list_gifs_panda[this.num_panda];
		
		this.num_panda++;
		
		a_panda.setCycleCount(javafx.animation.Animation.INDEFINITE);
		a_panda.setDelay(Duration.millis(delay));
		a_panda.play();

		ImageView panda = a_panda.getView();
		panda.setScaleX(0.7);
		panda.setScaleY(0.8);

		panda.setX(HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left - 25);
		panda.setY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up - 28);

		return panda;
	}
	
	public ImageView create_ghost(int i) {
		Animation a_ghost = this.list_gifs_ghost[this.num_ghost];
		ImageView ghost;
		
		this.num_ghost++;
		
		a_ghost.setCycleCount(javafx.animation.Animation.INDEFINITE);
		a_ghost.play();
		ghost = a_ghost.getView();
		ghost.setScaleX(0.08);
		ghost.setScaleY(0.08);
		
		ghost.setOpacity(0.82);
		ghost.setTranslateX(d.getGhost().get(i).hitboxGhost.center.x - 500);
		ghost.setTranslateY(d.getGhost().get(i).hitboxGhost.center.y - 500);

		return ghost;
	}
	
	public ImageView create_red_ghost(int i) {
		Animation a_red_ghost = this.list_gifs_red_ghost[this.num_red_ghost];
		ImageView red_ghost;
		
		this.num_red_ghost++;
		
		a_red_ghost.setCycleCount(javafx.animation.Animation.INDEFINITE);
		a_red_ghost.play();
		red_ghost = a_red_ghost.getView();
		red_ghost.setScaleX(0.08);
		red_ghost.setScaleY(0.08);
		
		red_ghost.setOpacity(0.82);
		red_ghost.setTranslateX(d.getGhost().get(i).hitboxGhost.center.x - 500);
		red_ghost.setTranslateY(d.getGhost().get(i).hitboxGhost.center.y - 500);
		
		return red_ghost;
	}
	
	public ImageView create_birds(int j) {
		Animation a_birds = this.list_gifs_birds[this.num_birds];
		ImageView birds;
		
		this.num_birds++;
		
		a_birds.setCycleCount(javafx.animation.Animation.INDEFINITE);
		a_birds.play();

		birds = a_birds.getView();
		birds.setScaleX(0.3);
		birds.setScaleY(0.3);

		birds.setX(d.getBirdsX());
		birds.setY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up + 20);
		return birds;
	}
	
	public ImageView create_big_bird(int j) {
		Animation a_big_bird = this.list_gifs_big_bird[this.num_big_bird];
		ImageView big_bird;
		
		this.num_big_bird++;
		
		a_big_bird.setCycleCount(javafx.animation.Animation.INDEFINITE);
		a_big_bird.play();

		big_bird = a_big_bird.getView();
		big_bird.setScaleX(0.3);
		big_bird.setScaleY(0.3);
		big_bird.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);

		big_bird.setX(d.getBig_birdX());
		big_bird.setY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up);

		return big_bird;
	}
	
	public ImageView create_egg(int i, int j) {
		Animation a_egg = this.list_gifs_egg[this.num_egg];

		this.num_egg++;

		a_egg.setCycleCount(javafx.animation.Animation.INDEFINITE);
		a_egg.play();

		ImageView egg = a_egg.getView();
		egg.setScaleX(0.15);
		egg.setScaleY(0.14);

		egg.setX(HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left - 265);
		egg.setY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up - 273);


		return egg;
	}
	
	public ImageView create_dragon(int j) {
		Animation a_dragon = this.list_gifs_dragon[this.num_dragon];
		ImageView dragon;
		
		this.num_big_bird++;
		
		a_dragon.setCycleCount(javafx.animation.Animation.INDEFINITE);
		a_dragon.play();

		dragon = a_dragon.getView();
		dragon.setScaleX(0.3);
		dragon.setScaleY(0.3);
		dragon.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);

		dragon.setX(d.getBig_birdX()-100);
		d.stepBird(true);
		dragon.setY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_up);

		return dragon;
	}
	
	public int getNum_id_tab() {
		return num_id_tab;
	}



	public ImageView create_explosion(int i,int j, int num_tab) {
		Animation a_explosion = list_gifs_explosion[(num_explosion++) % (HardCodedParameters.nb_bombe_max*20)];
		a_explosion.setCycleCount(1);
		a_explosion.play();
		a_explosion.getView().setTranslateX(HardCodedParameters.size_square * i
				+ HardCodedParameters.limit_map_border_left + HardCodedParameters.padding_explosion_x);
		a_explosion.getView().setTranslateY(HardCodedParameters.size_square * j
				+ HardCodedParameters.limit_map_border_up + HardCodedParameters.padding_explosion_y);
		
		if (num_tab == 0) {
			num_id_tab++;
			if (num_id_tab == HardCodedParameters.nb_bombe_max)
				num_id_tab = 0;
			int localv = num_id_tab;
			a_explosion.setOnFinished((finish)->v.set_finished_explode(localv));
		}
		return a_explosion.getView();
	}
	
	public ImageView create_heart(double x, double y, double scale) {
		Image image_heart= new Image("file:src/img/heart1.png");
		ImageView heart = new ImageView(image_heart);
		
		heart.setScaleX(scale);
		heart.setScaleY(scale);
		
		heart.setTranslateX(x);
		heart.setTranslateY(y);
		
		return heart;
	}
	
	public ImageView create_key(int i, int j) {
		Image image_key= new Image("file:src/img/key.png");
		ImageView key = new ImageView(image_key);
		
		key.setScaleX(0.085);
		key.setScaleY(0.085);
		
		key.setId(i+"key"+j);
		
		key.setTranslateX(HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left - 238);
		key.setTranslateY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_left - 558);
		
		return key;
	}
	
	public ImageView create_indications() {
		Image image_indic= new Image("file:src/img/tuto.png");
		ImageView indic = new ImageView(image_indic);
		
		indic.setScaleX(0.24);
		indic.setScaleY(0.24);
		
		indic.setTranslateX(-640);
		indic.setTranslateY(-945);
		
		return indic;
	}
	
	public ImageView create_heart_bonus(int i, int j) {
		Image image_heart= new Image("file:src/img/heart1.png");
		ImageView heart = new ImageView(image_heart);
		
		heart.setScaleX(0.05);
		heart.setScaleY(0.05);
		
		heart.setId(i+"heart"+j);
		
		heart.setTranslateX(HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left - 297);
		heart.setTranslateY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_left - 586);
		
		return heart;
	}
	
	public ImageView create_explosive_range_bonus(int i, int j) {
		Image image_expl= new Image("file:src/img/explosion.png");
		ImageView expl = new ImageView(image_expl);
		
		expl.setScaleX(0.22);
		expl.setScaleY(0.22);
		
		expl.setId(i+"expl_range"+j);
		
		expl.setTranslateX(HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left - 127);
		expl.setTranslateY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_left - 450);
		
		return expl;
	}
	
	public ImageView create_explosive_bonus(int i, int j) {
		Image image_expl= new Image("file:src/img/bombebonus.png");
		ImageView expl = new ImageView(image_expl);
		
		expl.setScaleX(0.07);
		expl.setScaleY(0.07);
		
		expl.setId(i+"expl_bonus"+j);
		
		expl.setTranslateX(HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left - 287);
		expl.setTranslateY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_left - 600);
		
		return expl;
	}
	
	public ImageView create_speed(int i, int j) {
		Image image_speed= new Image("file:src/img/speed.png");
		ImageView speed = new ImageView(image_speed);
		
		speed.setScaleX(0.085);
		speed.setScaleY(0.085);
		
		speed.setId(i+"speed"+j);
		
		speed.setTranslateX(HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left - 238);
		speed.setTranslateY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_left - 558);
		
		return speed;
	}

	
	private Node create_door(int i, int j) {
		Image image_door= new Image("file:src/img/door.png");
		ImageView door = new ImageView(image_door);
		
		door.setScaleX(0.28);
		door.setScaleY(0.22);
		
		door.setId(i+"door"+j);
		
		door.setTranslateX(HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left - 58);
		door.setTranslateY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_left - 420);
		
		return door;
	}

	public ImageView create_game_over(double scale) {
		Image image_go = new Image("file:src/img/gameover.jpg");
		ImageView go = new ImageView(image_go);
		
		go.setScaleX(scale);
		go.setScaleY(scale);
		
		go.setTranslateX(320);
		go.setTranslateY(200);
		
		return go;
	}


	public Node create_door_gif(int i, int j) {
		ImageView door;
		
		a_door.setCycleCount(1);
		a_door.pause();
		
		door = a_door.getView();
		
		door.setScaleX(0.25);
		door.setScaleY(0.22);

		door.setId(i+"door"+j);
		
		door.setTranslateX(HardCodedParameters.size_square * i + HardCodedParameters.limit_map_border_left - 653);
		door.setTranslateY(HardCodedParameters.size_square * j + HardCodedParameters.limit_map_border_left - 682);
		
		return door;
	}
	
	public void start_door_gif() {
		this.a_door.play();
	}
	
	public ImageView create_map1() {
		Image image_map = new Image("file:src/img/mapniveau1.jpg");
		// Affichage de la map + position definitive de la map
		ImageView map = new ImageView(image_map);
		
		map.setTranslateX(395);
		map.setTranslateY(75);
		
		
		return map;
	}
	
	public ImageView create_map2() {
		Image image_map = new Image("file:src/img/mapniveau2.jpg");
		// Affichage de la map + position definitive de la map
		ImageView map = new ImageView(image_map);
		
		map.setTranslateX(395);
		map.setTranslateY(75);
		
		
		return map;
	}
	
	public ImageView create_map3() {
		Image image_map = new Image("file:src/img/mapniveau3.jpg");
		// Affichage de la map + position definitive de la map
		ImageView map = new ImageView(image_map);
		
		map.setTranslateX(395);
		map.setTranslateY(75);
		
		
		return map;
	}
	
	public ImageView create_avatar_run(boolean shadow) {
		Animation a_run = this.list_gifs_run_avatar[this.num_run_avatar];
		ImageView run;
		
		this.num_run_avatar++;
		
		a_run.setCycleCount(javafx.animation.Animation.INDEFINITE);
		a_run.play();

		run = a_run.getView();
		run.setScaleX(HardCodedParameters.size_avatar_view);
		run.setScaleY(HardCodedParameters.size_avatar_view);
		run.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);

		if (shadow) run.setEffect(new Shadow());

		return run;
	}
	
	public ImageView create_avatar_idle(boolean shadow) {
		Animation a_idle = this.list_gifs_idle_avatar[this.num_idle_avatar];
		ImageView idle;
		
		this.num_idle_avatar++;
		
		a_idle.setCycleCount(javafx.animation.Animation.INDEFINITE);
		a_idle.play();

		idle = a_idle.getView();
		idle.setScaleX(HardCodedParameters.size_avatar_view);
		idle.setScaleY(HardCodedParameters.size_avatar_view);
		idle.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);

		if (shadow) idle.setEffect(new Shadow());

		return idle;
	}
	
	public Node create_end() {
		ImageView end;
		
		a_end.setCycleCount(-1);
		a_end.play();
		
		end = a_end.getView();
		
		end.setScaleX(0.5);
		end.setScaleY(0.5);
		
		end.setTranslateX(-100);
		end.setTranslateY(140);
		return end;
	}
	
}
