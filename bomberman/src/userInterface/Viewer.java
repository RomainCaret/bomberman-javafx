/* ******************************************************
 * Simulator alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: userInterface/Viewer.java 2015-03-09 buixuan.
 * ******************************************************/
package userInterface;

import tools.HardCodedParameters;

import specifications.ViewerService;
import specifications.ReadService;
import specifications.RequireReadService;
import specifications.StartEngineService;
import specifications.RequireStartEngineService;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import bodies.Ghost;
import data.Data;
import data.Trash;
import gestionson.Sound;
import gif.AnimatedGif;
import gif.Animation;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import objets.Bombe;
import objets.Wall;

public class Viewer implements ViewerService, RequireReadService, RequireStartEngineService {
	private ReadService data;
	private StartEngineService startEngine;

	private ImageView heroesAvatarRun;
	private ImageView heroesAvatarIdle;
	private ImageView heroesShadowRun;
	private ImageView heroesShadowIdle;
	private ImageView map;
	private ImageView gameover;
	private Animation[] list_gifs_bombe;
	private ArrayList<ImageView> list_bombe;
	
	private ImageView[][] list_explosions_delete;
	private ImageView birds, big_bird;

	private ArrayList<Integer> finished_explode;
	private int cpt_gif = 0;

	private boolean running = false;

	private long last_frame = 0;

	private Rectangle heroeHitbox;

	private Group panel;
	private VBox end;
	
	private Decor d;
	private ImageView[] image_coeurs;
	private int cpt_heart;
	private boolean ingameover = false;
	private Blend blend;
	private ArrayList<ImageView> ghost;
	private ArrayList<Rectangle> ghostHitbox;
	private boolean got_key;

	public Viewer() {
		this.finished_explode = new ArrayList<Integer>();
		// Chagement des GIF Bombe en Transition + List_bombe
		this.list_explosions_delete = new ImageView[HardCodedParameters.nb_bombe_max][HardCodedParameters.nb_bombe_max*20];
		this.list_bombe = new ArrayList<ImageView>();
		this.list_gifs_bombe = AnimatedGif.create_tab_gif("file:src/img/bombe.gif", 3800,HardCodedParameters.nb_bombe_max );
		
		this.image_coeurs = new ImageView[HardCodedParameters.nb_coeurs_max];
		this.cpt_heart = HardCodedParameters.nb_coeurs_max;
		
		this.blend = new Blend();
		
		blend.setMode(BlendMode.COLOR_BURN);
		this.ghost = new ArrayList<ImageView>();
		this.ghostHitbox = new ArrayList<Rectangle>();
	}

	@Override
	public void bindReadService(ReadService service) {
		data = service;
	}

	@Override
	public void bindStartEngineService(StartEngineService service) {
		startEngine = service;
	}

	public void init() {
		this.init_level_1();
	}
	@Override
	public void init_level_1() {
		data.set_level_1();
		this.got_key = false;
		if (this.d == null)
			this.d = new Decor(this, (Data) data);
		else
			this.d.reset();
		
		Group new_panel = new Group();
		List<Wall> crates = data.getList_wall();
		ObservableList<Node> list_children = new_panel.getChildren();
		
		//Position map + ajout map
		this.map = d.create_map1();
		list_children.add(0, this.map);
		
		//Ajout de l'avatar du player
		this.heroesAvatarRun = d.create_avatar_run(false);
		this.heroesShadowRun = d.create_avatar_run(true);
		
		this.heroesAvatarIdle = d.create_avatar_idle(false);
		this.heroesShadowIdle = d.create_avatar_idle(true);
		
		//Ajout objects + door
		list_children.addAll(d.createObjects(data.getMap_wall_crate()));
		
		//Ajout tuto
		list_children.add(d.create_indications());
		
		//Ajout Crates
		list_children.addAll(d.createImageCrate(crates));

		// Ajout eau
		int delay = 900;
		list_children.add(d.create_eau(3, 3, delay)); //Position i,j,temps d'attente entre 2 jets d'eau
		list_children.add(d.create_eau(3, 5, delay));
		list_children.add(d.create_eau(3, 7, delay));
		list_children.add(d.create_eau(3, 9, delay)); 
		list_children.add(d.create_eau(5, 9, delay));
		list_children.add(d.create_eau(7, 9, delay));
		list_children.add(d.create_eau(9, 9, delay));
		list_children.add(d.create_eau(9, 7, delay));
		list_children.add(d.create_eau(9, 5, delay));
		list_children.add(d.create_eau(9, 3, delay));
		list_children.add(d.create_eau(7, 3, delay));
		list_children.add(d.create_eau(5, 3, delay)); 
		

		// Ajout panda
		list_children.add(d.create_panda(5, 7, 2200)); //Position i,j,delay animation
		list_children.add(d.create_panda(7, 5, 0));

		// Ajout Ghost
		int cpt_ghost = 0;
		this.ghost.clear();
		for (Ghost g : data.getGhost()) {
			ImageView i_ghost =  d.create_ghost(cpt_ghost++);
			this.ghost.add(i_ghost);
			i_ghost.setVisible(true);
			list_children.add(i_ghost);
		}

		// Ajout oiseaux
		this.birds = d.create_birds(3); //Position j
		
		list_children.add(this.birds); 

		// Ajout oiseaux
		this.big_bird =d.create_big_bird(3); //Position j
		list_children.add(this.big_bird);

		// Ajout du joueur
		list_children.add(heroesShadowIdle);
		list_children.add(heroesAvatarIdle);
		
		this.running = false;

		//Ajout des coeurs
		int separateur_coeur = 60;
		int depart = -190;
		for(int val_coeurs =0;val_coeurs < HardCodedParameters.nb_coeurs_max;val_coeurs++) {
			this.image_coeurs[val_coeurs] = d.create_heart(depart, -200, 0.1);
			depart = depart + separateur_coeur;
			list_children.add(this.image_coeurs[val_coeurs]);
		}
		

		//Ajout de la Hitbox
		if (HardCodedParameters.hitbox_appears) {
			this.heroeHitbox = new Rectangle(data.getPlayer().hitboxPlayer.top_left.x,
					data.getPlayer().hitboxPlayer.top_left.y, HardCodedParameters.size_hitbox_body_width,
					HardCodedParameters.size_hitbox_body_height);
			heroeHitbox.setFill(Color.BLUE);
			list_children.add(heroeHitbox);
			this.ghostHitbox.clear();
			for (Ghost g : data.getGhost()) {
				
				Rectangle rec = new Rectangle(g.hitboxGhost.top_left.x,
						g.hitboxGhost.top_left.y, HardCodedParameters.size_hitbox_body_width,
						HardCodedParameters.size_hitbox_body_height);
				this.ghostHitbox.add(rec);
				rec.setFill(Color.BLACK);
				list_children.add(rec);
			}
		}
		this.gameover = d.create_game_over(2);
		this.cpt_heart = HardCodedParameters.nb_coeurs_max;

		this.panel = new_panel;
	}
	
	public void init_level_2(boolean reset) {
		data.set_level_2(reset);
		this.got_key = false;
		this.d.reset();
		
		Group new_panel = new Group();
		List<Wall> crates = data.getList_wall();
		ObservableList<Node> list_children = new_panel.getChildren();
		
		//Position map + ajout map
		this.map = d.create_map2();
		list_children.add(0, this.map);
		
		//Ajout de l'avatar du player
		this.heroesAvatarRun = d.create_avatar_run(false);
		this.heroesShadowRun = d.create_avatar_run(true);
		
		this.heroesAvatarIdle = d.create_avatar_idle(false);
		this.heroesShadowIdle = d.create_avatar_idle(true);
		
		//Ajout objects + door
		list_children.addAll(d.createObjects(data.getMap_wall_crate()));
		
		//Ajout Crates
		list_children.addAll(d.createImageCrate(crates));

		// Ajout eau
		list_children.add(d.create_eau(5, 5, 4000)); //Position i,j,temps d'attente entre 2 jets d'eau
		list_children.add(d.create_eau(7, 7, 4000)); 
		
		// Ajout feu
		list_children.add(d.create_feu(5, 3)); //Position i,j
		list_children.add(d.create_feu(9, 3));
		list_children.add(d.create_feu(7, 9));
		list_children.add(d.create_feu(9, 7));
		list_children.add(d.create_feu(3, 9));
		list_children.add(d.create_feu(3, 5));

		// Ajout panda
		list_children.add(d.create_panda(5, 7, 2200)); //Position i,j,delay animation
		list_children.add(d.create_panda(7, 5, 0));

		// Ajout Ghost
		int cpt_ghost = 0;
		this.ghost.clear();
		for (Ghost g : data.getGhost()) {
			ImageView i_ghost =  d.create_ghost(cpt_ghost++);
			i_ghost.setVisible(true);
			this.ghost.add(i_ghost);
			list_children.add(i_ghost);
		}

		// Ajout oiseaux
		this.birds = d.create_birds(3); //Position j
		
		list_children.add(this.birds); 

		// Ajout oiseaux
		this.big_bird =d.create_big_bird(3); //Position j
		list_children.add(this.big_bird);

		// Ajout du joueur
		list_children.add(heroesShadowIdle);
		list_children.add(heroesAvatarIdle);
		
		this.running = false;

		//Ajout des coeurs
		int separateur_coeur = 60;
		int depart = -190;
		for(int val_coeurs =0;val_coeurs < HardCodedParameters.nb_coeurs_max;val_coeurs++) {
			this.image_coeurs[val_coeurs] = d.create_heart(depart, -200, 0.1);
			depart = depart + separateur_coeur;
			list_children.add(this.image_coeurs[val_coeurs]);
		}
		

		//Ajout de la Hitbox
		if (HardCodedParameters.hitbox_appears) {
			this.heroeHitbox = new Rectangle(data.getPlayer().hitboxPlayer.top_left.x,
					data.getPlayer().hitboxPlayer.top_left.y, HardCodedParameters.size_hitbox_body_width,
					HardCodedParameters.size_hitbox_body_height);
			heroeHitbox.setFill(Color.BLUE);
			list_children.add(heroeHitbox);
			this.ghostHitbox.clear();
			for (Ghost g : data.getGhost()) {
				Rectangle rec = new Rectangle(g.hitboxGhost.top_left.x,
						g.hitboxGhost.top_left.y, HardCodedParameters.size_hitbox_body_width,
						HardCodedParameters.size_hitbox_body_height);
				rec.setFill(Color.BLACK);
				this.ghostHitbox.add(rec);
				list_children.add(rec);
			}
		}
		this.gameover = d.create_game_over(2);
		this.cpt_heart = HardCodedParameters.nb_coeurs_max;

		this.panel = new_panel;
	}
	
	public void init_level_3(boolean reset) {
		data.set_level_3(reset);
		this.got_key = false;
		this.d.reset();
		
		Group new_panel = new Group();
		List<Wall> crates = data.getList_wall();
		ObservableList<Node> list_children = new_panel.getChildren();
		
		//Position map + ajout map
		this.map = d.create_map3();
		list_children.add(0, this.map);
		
		//Ajout de l'avatar du player
		this.heroesAvatarRun = d.create_avatar_run(false);
		this.heroesShadowRun = d.create_avatar_run(true);
		
		this.heroesAvatarIdle = d.create_avatar_idle(false);
		this.heroesShadowIdle = d.create_avatar_idle(true);
		
		//Ajout objects + door
		list_children.addAll(d.createObjects(data.getMap_wall_crate()));
		
		//Ajout Crates
		list_children.addAll(d.createImageCrate(crates));
		
		// Ajout feu
		list_children.add(d.create_feu(5, 3)); //Position i,j
		list_children.add(d.create_feu(9, 3));
		list_children.add(d.create_feu(7, 9));
		list_children.add(d.create_feu(9, 7));
		list_children.add(d.create_feu(3, 9));
		list_children.add(d.create_feu(3, 5));

		// Ajout oeufs
		list_children.add(d.create_egg(5, 7)); //Position i,j,delay animation
		list_children.add(d.create_egg(7, 5));

		// Ajout Ghost
		this.ghost.clear();
		int cpt_red_ghost = 0;
		for (Ghost g : data.getGhost()) {
			ImageView i_ghost =  d.create_red_ghost(cpt_red_ghost++);
			this.ghost.add(i_ghost);
			i_ghost.setVisible(true);
			list_children.add(i_ghost);
		}

		// Ajout oiseaux
		this.birds = d.create_birds(3); //Position j
		this.birds.setVisible(false);
		list_children.add(this.birds); 

		// Ajout oiseaux
		this.big_bird =d.create_dragon(-4); //Position j
		list_children.add(this.big_bird);

		// Ajout du joueur
		list_children.add(heroesShadowIdle);
		list_children.add(heroesAvatarIdle);
		
		this.running = false;

		//Ajout des coeurs
		int separateur_coeur = 60;
		int depart = -190;
		for(int val_coeurs =0;val_coeurs < HardCodedParameters.nb_coeurs_max;val_coeurs++) {
			this.image_coeurs[val_coeurs] = d.create_heart(depart, -200, 0.1);
			depart = depart + separateur_coeur;
			list_children.add(this.image_coeurs[val_coeurs]);
		}
		

		//Ajout de la Hitbox
		if (HardCodedParameters.hitbox_appears) {
			this.heroeHitbox = new Rectangle(data.getPlayer().hitboxPlayer.top_left.x,
					data.getPlayer().hitboxPlayer.top_left.y, HardCodedParameters.size_hitbox_body_width,
					HardCodedParameters.size_hitbox_body_height);
			heroeHitbox.setFill(Color.BLUE);
			list_children.add(heroeHitbox);
			this.ghostHitbox.clear();
			for (Ghost g : data.getGhost()) {
				
				Rectangle rec = new Rectangle(g.hitboxGhost.top_left.x,
						g.hitboxGhost.top_left.y, HardCodedParameters.size_hitbox_body_width,
						HardCodedParameters.size_hitbox_body_height);
				this.ghostHitbox.add(rec);
				rec.setFill(Color.BLACK);
				list_children.add(rec);
			}
		}
		this.gameover = d.create_game_over(2);
		this.cpt_heart = HardCodedParameters.nb_coeurs_max;

		this.panel = new_panel;
	}

	@Override
	public void startViewer() {
		startEngine.start();
	}

	@Override
	public Node getPanel(long l) {
		if (ingameover) {
			return this.end;
		}
		
		if (l - this.last_frame > HardCodedParameters.viewer_frame) {
			last_frame = l;
		} else {
			// System.out.println("Frame non calculé");
			return this.panel;
		}
		
		ImageView bombe;
		bombe = null;
		list_bombe.clear();
		Animation tmpBombeGif;

		ArrayList<Bombe> list_pos = data.getList_bombe();

		ObservableList<Node> list_children = this.panel.getChildren();
		
		if (data.getPlayer().coeur <= 0) {
			list_children.clear();
			data.addSound(Sound.Death);
			switch (data.getLevelplayer()) {		
			case 1: {
				this.init_level_1();
				break;
			}
			case 2:{
				this.init_level_2(true);
				break;
			}
			case 3:{
				this.init_level_3(true);
				break;
			}
		}
		return this.panel;
		}
		
		/*Check objects*/
		int i_joueur = Math.round(Math.round((data.getPlayer().hitboxPlayer.top_left.x-15 - HardCodedParameters.limit_map_border_left)
				/ HardCodedParameters.size_square));
		int j_joueur = Math.round(Math.round((data.getPlayer().hitboxPlayer.top_left.y-10 - HardCodedParameters.limit_map_border_up)
				/ HardCodedParameters.size_square));
		
		switch (data.getMap_wall_crate()[i_joueur][j_joueur]) {
		case 3: {
			list_children.remove(panel.lookup("#"+i_joueur+"key"+j_joueur));
			data.getMap_wall_crate()[i_joueur][j_joueur] = 0;
			
			if (this.got_key == false) {
				data.addSound(Sound.Door);
				d.start_door_gif();
			}
			this.got_key = true;
			break;
		}
		case 4: {
			data.addSound(Sound.Item);
			list_children.remove(panel.lookup("#"+i_joueur+"speed"+j_joueur));
			data.getMap_wall_crate()[i_joueur][j_joueur] = 0;
			data.speed_up_player();
			break;
		}
		
		case 5: {
			data.addSound(Sound.Item);
			list_children.remove(panel.lookup("#"+i_joueur+"heart"+j_joueur));
			data.getMap_wall_crate()[i_joueur][j_joueur] = 0;
			if (data.getPlayer().coeur < HardCodedParameters.nb_coeurs_max)
				data.getPlayer().coeur++;
			break;
		}
		
		case 6: {
			data.addSound(Sound.Item);
			list_children.remove(panel.lookup("#"+i_joueur+"expl_range"+j_joueur));
			data.getMap_wall_crate()[i_joueur][j_joueur] = 0;
			data.upTaille_explo();
			break;
		}
		case 7: {
			data.addSound(Sound.Item);
			list_children.remove(panel.lookup("#"+i_joueur+"expl_bonus"+j_joueur));
			data.getMap_wall_crate()[i_joueur][j_joueur] = 0;
			data.explosive_up();
			break;
		}
		
		case 9: {
			if (this.got_key) {
				list_children.clear();
				switch (data.getLevelplayer()) {		
					case 1: {
						this.init_level_2(false);
						break;
					}
					case 2:{
						this.init_level_3(false);
						break;
					}
					case 3:{
						if (!ingameover) {
							startEngine.stop();
							data.son_end();
							ingameover = true;
						
							data.addSound(Sound.Enfants);
							VBox vb = new VBox();
							vb.setAlignment(Pos.CENTER);
							BackgroundImage bc = new BackgroundImage(new Image("file:src/img/Fin.jpg"), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, false, true));
			            	vb.setBackground(new Background(bc));
			            	vb.getChildren().add(d.create_end());
			            	this.end = vb;
						
							
						}
						break;
					}
				}
				return this.panel;
			}
			break;
		}
		
		default:
		}
		
		try {
			data.getAccess_bombe().acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// Fin check objects
		for (Bombe b_position : list_pos) {

			if (b_position.isAppeared() == false) {
				b_position.setBombeGif(list_gifs_bombe[cpt_gif++]);
				b_position.setAppeared(true);
				tmpBombeGif = b_position.getBombeGif();
				tmpBombeGif.setCycleCount(1);
				tmpBombeGif.play();
				bombe = tmpBombeGif.getView();

				bombe.setScaleX(0.07);
				bombe.setScaleY(0.07);
				// bombe.setScaleX(0.5);
				// bombe.setScaleY(0.5);
				bombe.setTranslateX(b_position.getPosition().x - 115 - HardCodedParameters.size_hitbox
						- HardCodedParameters.defaultWidth / 4);
				bombe.setTranslateY(b_position.getPosition().y - 55 - HardCodedParameters.size_hitbox
						- HardCodedParameters.defaultHeight / 3);
				if (cpt_gif >= HardCodedParameters.nb_bombe_max)
					cpt_gif = 0;

				bombe.toFront();
				list_children.add(bombe);

				if (HardCodedParameters.hitbox_appears) {
					b_position.setHitbox(new Circle(b_position.getPosition().x, b_position.getPosition().y,
							HardCodedParameters.size_hitbox, Color.RED));
					b_position.getHitbox().toFront();
					list_children.add(b_position.getHitbox());
				}

			}
			if (b_position.getJustSpawned() == false) {
				b_position.getBombeGif().getView().toBack();
			}
			if (b_position.getTime_before_explode() < HardCodedParameters.time_hitbox_bomb_disappeared) {
				if (HardCodedParameters.hitbox_appears) {
					list_children.remove(b_position.getHitbox());
				}
			}
		}

		data.getAccess_bombe().release();

		
		// Ajout Ghost
		for (int cpt_ghost = 0; cpt_ghost < this.ghost.size();cpt_ghost++) {
			ImageView i_ghost = this.ghost.get(cpt_ghost);
			Ghost g = data.getGhost().get(cpt_ghost);
			if (g.coeur == 0) {
				
				i_ghost.setVisible(false);
			}
			
			if (g.frames_invulnerable > 60 || (g.frames_invulnerable > 15 && g.frames_invulnerable < 40)) {
				i_ghost.setEffect(blend);
				
			}else {
				i_ghost.setEffect(null);
			}
			
			i_ghost.setTranslateX(g.hitboxGhost.center.x-297);
			i_ghost.setTranslateY(g.hitboxGhost.center.y-303);
			i_ghost.setNodeOrientation(g.orientation);
			
		}
		
		
		
		// this.heroesAvatar.setId("Player");
		this.heroesShadowIdle.setTranslateX(data.getHeroesPosition().x
				- (HardCodedParameters.size_hitbox_body_width / 2 + 15) - HardCodedParameters.decalage_width);
		this.heroesShadowIdle.setTranslateY(data.getHeroesPosition().y
				- (HardCodedParameters.size_hitbox_body_height * 1.8) - HardCodedParameters.decalage_height);
		this.heroesShadowIdle.setNodeOrientation(data.getOrientation());

		this.heroesShadowRun.setTranslateX(data.getHeroesPosition().x
				- (HardCodedParameters.size_hitbox_body_width / 2 + 15) - HardCodedParameters.decalage_width);
		this.heroesShadowRun.setTranslateY(data.getHeroesPosition().y
				- (HardCodedParameters.size_hitbox_body_height * 1.8) - HardCodedParameters.decalage_height);
		this.heroesShadowRun.setNodeOrientation(data.getOrientation());

		this.heroesAvatarRun.setTranslateX(data.getHeroesPosition().x
				- (HardCodedParameters.size_hitbox_body_width / 2 + 6) - HardCodedParameters.decalage_width);
		this.heroesAvatarRun.setTranslateY(data.getHeroesPosition().y
				- (HardCodedParameters.size_hitbox_body_height * 2 - 4) - HardCodedParameters.decalage_height);
		this.heroesAvatarRun.setNodeOrientation(data.getOrientation());

		this.heroesAvatarIdle.setTranslateX(data.getHeroesPosition().x
				- (HardCodedParameters.size_hitbox_body_width / 2 + 6) - HardCodedParameters.decalage_width);
		this.heroesAvatarIdle.setTranslateY(data.getHeroesPosition().y
				- (HardCodedParameters.size_hitbox_body_height * 2 - 4) - HardCodedParameters.decalage_height);
		this.heroesAvatarIdle.setNodeOrientation(data.getOrientation());

		if (data.getPlayer().frames_invulnerable > 60 || (data.getPlayer().frames_invulnerable > 15 && data.getPlayer().frames_invulnerable < 40)) {
			this.heroesAvatarIdle.setEffect(blend);
			this.heroesAvatarRun.setEffect(blend);
		}else {
			this.heroesAvatarIdle.setEffect(null);
			this.heroesAvatarRun.setEffect(null);
		}
		
		this.map.toBack();
		if (running == false && (data.getGoDown() || data.getGoLeft() || data.getGoRight() || data.getGoUp())) {
			list_children.remove(this.heroesAvatarIdle);
			list_children.remove(this.heroesShadowIdle);
			list_children.add(this.heroesShadowRun);
			list_children.add(this.heroesAvatarRun);
			running = true;
		}

		if (running == true && !(data.getGoDown() || data.getGoLeft() || data.getGoRight() || data.getGoUp())) {
			list_children.remove(this.heroesAvatarRun);
			list_children.remove(this.heroesShadowRun);
			list_children.add(this.heroesShadowIdle);
			list_children.add(this.heroesAvatarIdle);

			running = false;
		}
		if (HardCodedParameters.hitbox_appears) {
			this.heroeHitbox.setX(data.getPlayer().hitboxPlayer.top_left.x);
			this.heroeHitbox.setY(data.getPlayer().hitboxPlayer.top_left.y);
			
			// Ajout Ghost
			for (int cpt_ghost = 0; cpt_ghost < this.ghost.size();cpt_ghost++) {
				Rectangle i_ghost = this.ghostHitbox.get(cpt_ghost);
				Ghost g = data.getGhost().get(cpt_ghost);
				i_ghost.setX(g.hitboxGhost.top_left.x);
				i_ghost.setY(g.hitboxGhost.top_left.y);
				i_ghost.toFront();
			}
			this.heroeHitbox.toFront();
		}

		
		/*Gestion suppression des crate*/
		ArrayList<Trash> list_crate = data.getTo_delete();
		// Qui dit suppresion crates, dit explosion
		if (list_crate.isEmpty() == false) {
			int i = list_crate.get(0).getI();
			int j = list_crate.get(0).getJ();
			int[][] m = data.getMap_wall_crate();
			boolean wall1, wall2, wall3, wall4;

			wall1 = false;
			wall2 = false;
			wall3 = false;
			wall4 = false;
			int num_tab = 0;
			
			ImageView explosion = d.create_explosion(i, j, num_tab);
			int id_tab = d.getNum_id_tab();
			list_explosions_delete[id_tab][num_tab++] =explosion;
			list_children.add(explosion);
			for (int radius = 1; radius <= data.getTaille_explo(); radius++) {
				if (i + radius < HardCodedParameters.map_size && m[i + radius][j] == 1)
					wall1 = true;
				if (j + radius < HardCodedParameters.map_size && m[i][j + radius] == 1)
					wall2 = true;
				if (i - radius >= 0 && m[i - radius][j] == 1)
					wall3 = true;
				if (j - radius >= 0 && m[i][j - radius] == 1)
					wall4 = true;

				if (wall1 == false && i + radius < HardCodedParameters.map_size && m[i + radius][j] != 1) {
					explosion = d.create_explosion(i+radius, j, num_tab);
					list_explosions_delete[id_tab][num_tab++] = explosion;
					list_children.add(explosion);
				}
				if (wall2 == false && j + radius < HardCodedParameters.map_size && m[i][j + radius] != 1) {
					explosion = d.create_explosion(i, j+radius, num_tab);
					list_explosions_delete[id_tab][num_tab++] = explosion;
					list_children.add(explosion);
				}

				if (wall3 == false && i - radius >= 0 && m[i - radius][j] != 1) {
					explosion = d.create_explosion(i-radius, j, num_tab);
					list_explosions_delete[id_tab][num_tab++] = explosion;
					list_children.add(explosion);
				}
				if (wall4 == false && j - radius >= 0 && m[i][j - radius] != 1) {
					explosion = d.create_explosion(i, j-radius, num_tab);
					list_explosions_delete[id_tab][num_tab++] = explosion;
					list_children.add(explosion);
				}
			}
		}
		
		/* Suppression des explosions*/
		for (int num : finished_explode) {
			for (int i = 0 ;i < list_explosions_delete[num].length; i++) {
				if (list_explosions_delete[num][i] != null) {
					list_children.remove(list_explosions_delete[num][i]);
					list_explosions_delete[num][i] = null;
				}
			}
		}
		finished_explode.clear();
		
		
		/*Gestion Coeurs*/
		if(data.getPlayer().coeur < cpt_heart) {
			list_children.remove(this.image_coeurs[cpt_heart-1]);
			cpt_heart--;
		}
		
		if(data.getPlayer().coeur > cpt_heart) {
			list_children.add(this.image_coeurs[cpt_heart]);
			cpt_heart++;
		}
		
		
		
		/*Suppression des crates*/
		for (Trash trash : list_crate) {
			list_children.remove(trash.getTo_delete());
		}
		list_crate.clear();

		
		/* Gestion des oiseaux*/
		birds.setX(data.getBirdsX());
		birds.setNodeOrientation(data.getBirdOrientation() == NodeOrientation.LEFT_TO_RIGHT ? NodeOrientation.RIGHT_TO_LEFT : NodeOrientation.LEFT_TO_RIGHT);
		birds.toFront();

		big_bird.setX(data.getBig_birdX());
		big_bird.toFront();
		big_bird.setNodeOrientation(data.getBirdOrientation());

		
		
		//System.out.println("val :" +cpt_gif_explosion);
		//System.out.println(list_children.size());
		//System.out.println(data.getPlayer().coeur);
		return this.panel;
	}

	public void set_finished_explode(int cpt_tab_gif_explosion2) {
		finished_explode.add(cpt_tab_gif_explosion2);
		
	}
}
