/* ******************************************************
 * Simulator alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: tools/HardCodedParameters.java 2015-03-09 buixuan.
 * ******************************************************/
package tools;

public class HardCodedParameters {
  //---HARD-CODED-PARAMETERS---//
  public static String defaultParamFileName = "in.parameters";
  
  public static int time_hitbox_bomb_disappeared = 20;

public static double decalage_width = 114;
public static double decalage_height = 129;

	public static long viewer_frame= 6000000; // Plus est grand moins de rafraichissement (Moteur graphique)
	
	public static double speed_init = 1.3; // Vitesse du player pour chaque step d'engine

	public static int taille_explosion_init = 1;

	public static int map_size = 13;

	public static double padding_explosion_x = -130;
	public static double padding_explosion_y = -130;

	public static double size_avatar_view = 0.23;

	public static double size_image_explosion = 0.18;

    public static int hit_bombe_frames = 47;

    public static int frames_after_hit = 100;

	public static int nb_coeurs_max =6;

	public static double enginePaceMillisSound = 20;

	public static double speedGhostInit = 0.5;

	public static int nb_bombe_max_init = 1;
	
	public static final int enginePaceMillis = 14; // Raffrachissement d'engine en ms (Moteur de jeu)

  public static final double size_hitbox_body_height = 25;
  public static final double size_hitbox_body_width = 25;

  public static final double size_square = 48;
  public static final double size_hitbox = 13;

  public static final boolean hitbox_appears = true;
  
  public static final int nb_bombe_max = 40;
  public static final int defaultWidth = 900, defaultHeight = 900;
  public static final double limit_map_border_left = 450, limit_map_border_right = 1065;
  public static final double limit_map_border_up = 125, limit_map_border_down = 745;

  
  
  public static final double resolutionShrinkFactor = 0.95,
                             userBarShrinkFactor = 0.25,
                             menuBarShrinkFactor = 0.5,
                             logBarShrinkFactor = 0.15,
                             logBarCharacterShrinkFactor = 0.1175,
                             logBarCharacterShrinkControlFactor = 0.01275,
                             menuBarCharacterShrinkFactor = 0.175;
  public static final int displayZoneXStep = 5,
                          displayZoneYStep = 5,
                          displayZoneXZoomStep = 5,
                          displayZoneYZoomStep = 5;
  public static final double displayZoneAlphaZoomStep = 0.98;

  //---MISCELLANOUS---//
  public static final Object loadingLock = new Object();
  public static final String greetingsZoneId = String.valueOf(0xED1C7E),
                             simulatorZoneId = String.valueOf(0x51E77E);
  
  public static <T> T instantiate(final String className, final Class<T> type){
    try{
      return type.cast(Class.forName(className).newInstance());
    } catch(final InstantiationException e){
      throw new IllegalStateException(e);
    } catch(final IllegalAccessException e){
      throw new IllegalStateException(e);
    } catch(final ClassNotFoundException e){
      throw new IllegalStateException(e);
    }
  }
}
