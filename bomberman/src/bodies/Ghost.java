package bodies;

import java.util.ArrayList;

import data.Data;
import data.Explosion;
import data.Position;
import gestionson.Sound;
import hitbox.Rectangle;
import javafx.geometry.NodeOrientation;
import tools.HardCodedParameters;

public class Ghost{
    public Rectangle hitboxGhost;
    public int frames_invulnerable;
    public int coeur;
    public NodeOrientation orientation;
    private Data d;
    
    public Ghost(Position p, int coeur, Data d){
        this.hitboxGhost = new Rectangle(p, HardCodedParameters.size_hitbox_body_width, 
        		HardCodedParameters.size_hitbox_body_height);
        this.coeur = coeur;
        this.frames_invulnerable = 0;
        this.d = d;
        orientation = NodeOrientation.INHERIT;
    }

    public void isHit() {
        ArrayList<Explosion> m = d.getMapHit();
        ArrayList<Explosion> m_tmp = (ArrayList<Explosion>) m.clone();

        if (frames_invulnerable > 0){
            for (Explosion explode : m_tmp) {
                explode.decrease_hit_frames();
                int still_hurt = explode.get_hit_frames();
                if (still_hurt == 0){
                    m.remove(explode);
                }
            }
            this.frames_invulnerable--;
            return ;
        }

        int i = Math.round(Math.round((hitboxGhost.top_left.x-15 - HardCodedParameters.limit_map_border_left)
        / HardCodedParameters.size_square));
        int j = Math.round(Math.round((hitboxGhost.top_left.y-10 - HardCodedParameters.limit_map_border_up)
        / HardCodedParameters.size_square));

        boolean is_hit = false;
        for (Explosion explode : m_tmp) {
           explode.decrease_hit_frames();
            int still_hurt = explode.get_hit_frames();
            if (still_hurt == 0){
                m.remove(explode);
            }else{
                if (explode.get_map()[i][j]){        
                    is_hit = true;
                }
            }
        }
        if (is_hit){
            frames_invulnerable = HardCodedParameters.frames_after_hit;
            coeur--;
        }        
    }
}
