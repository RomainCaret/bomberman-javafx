package bodies;

import data.Position;
import hitbox.Rectangle;
import tools.HardCodedParameters;

public class Zombie{
    public Rectangle hitboxPlayer;
    
    public Zombie(Position p){
        this.hitboxPlayer = new Rectangle(p, HardCodedParameters.size_hitbox_body_height, 
            HardCodedParameters.size_hitbox_body_width);
    }
}
