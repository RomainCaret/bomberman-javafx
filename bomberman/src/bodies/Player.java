package bodies;

import java.util.ArrayList;

import collision.GestionCollide;
import data.Data;
import data.Explosion;
import data.Position;
import gestionson.Sound;
import hitbox.Rectangle;
import tools.HardCodedParameters;

public class Player{
    public Rectangle hitboxPlayer;
    public int frames_invulnerable;
    public int coeur;
    private Data d;
    
    public Player(Position p, Data d, int coeur){
        this.hitboxPlayer = new Rectangle(p, HardCodedParameters.size_hitbox_body_width, 
        		HardCodedParameters.size_hitbox_body_height);
        this.coeur = coeur;
        this.frames_invulnerable = 0;
        this.d = d;
    }

    public void isHit() {
        ArrayList<Explosion> m = d.getMapHit();
        ArrayList<Explosion> m_tmp = (ArrayList<Explosion>) m.clone();

        if (frames_invulnerable > 0){
            for (Explosion explode : m_tmp) {
                explode.decrease_hit_frames();
                int still_hurt = explode.get_hit_frames();
                if (still_hurt == 0){
                    m.remove(explode);
                }
            }
            this.frames_invulnerable--;
            return ;
        }

        
        boolean is_hit = false;
        
        for (Ghost g : d.getGhost()) {
        	if (g.coeur == 0)
        		continue;
	        if (GestionCollide.rectRectColliding(hitboxPlayer, g.hitboxGhost)) {
	        	is_hit = true;
	        	break;
	        }
        }
        int i = Math.round(Math.round((hitboxPlayer.top_left.x-15 - HardCodedParameters.limit_map_border_left)
        / HardCodedParameters.size_square));
        int j = Math.round(Math.round((hitboxPlayer.top_left.y-10 - HardCodedParameters.limit_map_border_up)
        / HardCodedParameters.size_square));

        
        for (Explosion explode : m_tmp) {
           explode.decrease_hit_frames();
            int still_hurt = explode.get_hit_frames();
            if (still_hurt == 0){
                m.remove(explode);
            }else{
                if (explode.get_map()[i][j]){        
                    is_hit = true;
                }
            }
        }
        if (is_hit){
        	if(d.getSoundEffect() != Sound.None)
        		d.addSound(Sound.HeroesGotHit);
        	else
        		d.setSoundEffect(Sound.HeroesGotHit);
            frames_invulnerable = HardCodedParameters.frames_after_hit;
            coeur--;
        }        
    }
}
