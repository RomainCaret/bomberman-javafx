package objets;

import data.Position;
import javafx.scene.image.ImageView;

public class Crate extends Wall {
	
	
	private int i;
	private int j;
	
	private ImageView crate;
	
    public Crate(Position top_left, double width, double height, int i, int j){
    	super(true, top_left, width, height);
    	this.i = i;
    	this.j = j;
    }

	public ImageView getCrate() {
		return crate;
	}

	public void setCrate(ImageView crate) {
		this.crate = crate;
	}

	public int getI() {
		return i;
	}

	public int getJ() {
		return j;
	}
}
