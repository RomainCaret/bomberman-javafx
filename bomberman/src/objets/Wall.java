package objets;

import data.Position;
import hitbox.Rectangle;

public class Wall extends Rectangle{
	private boolean breakable;
	
	public Wall(boolean breakable,Position top_left, double width, double height) {
		super(width, height,top_left);
		this.breakable = breakable;
		
	}

	public boolean isBreakable() {
		return breakable;
	}

	public void setBreakable(boolean breakable) {
		this.breakable = breakable;
	}

}
