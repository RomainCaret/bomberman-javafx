package objets;

import data.Position;
import gif.Animation;
import hitbox.Cercle;
import hitbox.HitboxI;
import javafx.scene.shape.Circle;
import specifications.BombeSevices;
import tools.HardCodedParameters;

public class Bombe implements BombeSevices{
	private Position position;
	private int time_before_explode;
	private final HitboxI hitboxBombe;
	private boolean just_spawned;
	private boolean appeared;
	private Animation BombeGif;
	private Circle hitbox = null;
	private int i;
	private int j;
	
	public Bombe(Position p,int i, int j) {
		this.position = p;
		this.hitboxBombe = new Cercle(p, HardCodedParameters.size_hitbox);
		this.time_before_explode = 175;
		this.just_spawned = true;
		this.appeared = false;
		this.i = i;
		this.j = j;
	}
	
	@Override
	public int getI() {
		return i;
	}

	@Override
	public int getJ() {
		return j;
	}

	@Override
	public Circle getHitbox() {
		return hitbox;
	}
	
	@Override
	public void setHitbox(Circle hitbox) {
		this.hitbox = hitbox;
	}
	
	@Override
	public Animation getBombeGif() {
		return BombeGif;
	}
	public void setBombeGif(Animation bombeGif) {
		BombeGif = bombeGif;
	}
	
	@Override
	public boolean isAppeared() {
		return appeared;
	}
	
	@Override
	public void setAppeared(boolean appeared) {
		this.appeared = appeared;
	}
	@Override
	public Position getPosition() {
		return position;
	}
	@Override
	public void setPosition(Position position) {
		this.position = position;
	}
	@Override
	public int getTime_before_explode() {
		return time_before_explode;
	}
	@Override
	public HitboxI getHitboxBombe(){
		return hitboxBombe;
	}
	@Override
	public void setTime_before_explode(int time_before_explode) {
		this.time_before_explode = time_before_explode;
	}

	@Override
	public boolean getJustSpawned(){
		return this.just_spawned;
	}

	@Override
	public void notJustSpawned(){
		this.just_spawned = false;
	}
	
	
}
