package specifications;

import data.Position;
import gif.Animation;
import hitbox.HitboxI;
import javafx.scene.shape.Circle;

public interface BombeSevices {

	public Position getPosition();

	public void setPosition(Position position);

	public int getTime_before_explode();

	public void setTime_before_explode(int time_before_explode);

	public HitboxI getHitboxBombe();

	public boolean getJustSpawned();

	public void notJustSpawned();
	
	public Circle getHitbox();

	void setHitbox(Circle hitbox);

	Animation getBombeGif();

	boolean isAppeared();

	void setAppeared(boolean appeared);
	
	public int getI();
	
	public int getJ();
}
