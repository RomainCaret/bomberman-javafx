/* ******************************************************
 * Simulator alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: specifications/ReadService.java 2015-03-09 buixuan.
 * ******************************************************/
package specifications;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import bodies.Ghost;
import bodies.Player;
import data.Position;
import data.Trash;
import gestionson.Sound;
import javafx.geometry.NodeOrientation;
import javafx.scene.image.ImageView;
import objets.Bombe;
import objets.Objets;
import objets.Wall;

public interface ReadService {
  public Position getHeroesPosition();
  public int getStepNumber();
  public NodeOrientation getOrientation();
  public boolean getGoLeft();
  public boolean getGoRight();
  public boolean getGoUp();
  public boolean getGoDown();
  public ArrayList<Bombe> getList_bombe();
  public Player getPlayer();
List<Wall> getList_wall();
public Semaphore getAccess_bombe();
public ArrayList<Trash> getTo_delete();
public int getTaille_explo() ;
public int[][] getMap_wall_crate();
public double getBig_birdX();
public double getBirdsX();
public NodeOrientation getBirdOrientation();
public Sound getSoundEffect();
public List<Ghost> getGhost();
public void set_level_1();
/**
 * @return the levelplayer
 */
int getLevelplayer();
void setLevelplayer(int levelplayer);
public void set_level_2(boolean reset);
public void set_level_3(boolean reset);
/**
 * @return the speed_player
 */
double getSpeed_player();
/**
 * @param speed_player the speed_player to set
 */
void speed_up_player();
void upTaille_explo();
void explosive_up();
public void stepBird(boolean dragon);
void son_end();
double getSpeed_ghost();
void addSound(Sound sound);
}
