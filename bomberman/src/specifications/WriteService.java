/* ******************************************************
 * Simulator alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: specifications/WriteService.java 2015-03-09 buixuan.
 * ******************************************************/
package specifications;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import data.Position;
import gestionson.SonJeu;
import gestionson.Sound;
import javafx.geometry.NodeOrientation;
import javafx.scene.image.ImageView;
import objets.Bombe;
import objets.Wall;

public interface WriteService {
  public void setHeroesPosition(Position p);
  public void setStepNumber(int n);
  public void setOrientation(NodeOrientation orientation);
  public void setGoLeft(boolean goLeft);
  public void setGoRight(boolean goRight);
  public void setGoUp(boolean goUp);
  public void setGoDown(boolean goDown);
  public void addBomb();
  public void deleteBombe(Bombe bombe);
  public void setList_wall(ArrayList<Wall> list_wall);
  public void setAccess_bombe(Semaphore access_bombe);
  public void clear_delete();
  public void add_delete(ImageView to_delete, int i, int j);

  public Sound setSoundEffect(Sound s);
/**
 * @param levelplayer the levelplayer to set
 */
void setLevelplayer(int levelplayer);
void bind_bruit_boucle(SonJeu b);
  
}
