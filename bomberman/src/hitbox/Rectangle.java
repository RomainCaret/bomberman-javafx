package hitbox;

import data.Position;

public class Rectangle {
    public Position center;
    public Position top_left;
    public double width;
    public double height;

    public Rectangle(Position center, double width, double height){
        this.center = center;
        this.height = height;
        this.width = width;
        this.top_left = new Position(center.x-width/2., center.y-height/2.);
    }
    
    public Rectangle(double width, double height, Position top_left){
        this.top_left = top_left;
        this.height = height;
        this.width = width;
        this.center = new Position(top_left.x+width/2., top_left.y+height/2.);
    }

	public void setPosition(Position p) {
		this.center = p;
        this.top_left = new Position(p.x-width/2., p.y-height/2.);
		
	}
	public String toString() {
		return "Rectangle("+top_left.toString()+", height : "+height+", width : "+width+")";
	}
}
