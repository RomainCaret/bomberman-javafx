package hitbox;

import data.Position;

public class Cercle implements HitboxI{
    public Position center;
    public double rayon;

    public Cercle(Position center, double rayon){
        this.center = center;
        this.rayon = rayon;
    }
}
