package gif;

import java.awt.image.BufferedImage;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import tools.HardCodedParameters;

public class AnimatedGif extends Animation implements Cloneable{
	
    public AnimatedGif( String filename, double durationMs) {
	    
    	GifDecoder d = new GifDecoder();
	    d.read( filename);

        Image[] sequence = new Image[ d.getFrameCount()];
        for( int i=0; i < d.getFrameCount(); i++) {
            WritableImage wimg = null;
            BufferedImage bimg = d.getFrame(i);
            sequence[i] = SwingFXUtils.toFXImage( bimg, wimg);

        }

        super.init( sequence, durationMs);
    }
    
    public static Animation[] create_tab_gif(String filename, double durationMs, int nb){
    	Animation[] tab_gif = new Animation[nb];
    	
    	GifDecoder d = new GifDecoder();
	    d.read( filename);

        Image[] sequence = new Image[ d.getFrameCount()];
        for( int i=0; i < d.getFrameCount(); i++) {
            WritableImage wimg = null;
            BufferedImage bimg = d.getFrame(i);
            sequence[i] = SwingFXUtils.toFXImage( bimg, wimg);

        }
    	
    	
    	for (int i = 0; i < nb; i++) {
			tab_gif[i] = new Animation(sequence, durationMs);
		}
    	return tab_gif;
    }
    public Object clone(){  
        try{  
            return super.clone();  
        }catch(Exception e){ 
            return null; 
        }
    }

}
