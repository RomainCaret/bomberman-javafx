package collision;

import java.util.ArrayList;
import java.util.List;

import data.Position;
import hitbox.Cercle;
import hitbox.Rectangle;
import objets.Bombe;
import objets.Wall;
import specifications.CollideServices;
import specifications.DataService;
import specifications.RequireDataService;
import tools.HardCodedParameters;

public class GestionCollide implements CollideServices, RequireDataService {
    DataService data;
    
    public GestionCollide(){} // signé : romain.
   
    @Override
    public boolean check_collide_player(double x, double y){
        ArrayList<Bombe> list_bombe = data.getList_bombe();
        List<Wall> list_wall = data.getList_wall();
        
        Rectangle hitboxPlayer = new Rectangle(new Position(x , y),
            HardCodedParameters.size_hitbox_body_width,HardCodedParameters.size_hitbox_body_height);
        
        
        for (Wall wall : list_wall) {
			if (rectRectColliding(wall, hitboxPlayer))
				return false;
		}
        for (Bombe bombe : list_bombe) {
        	if (bombe.getTime_before_explode() <= HardCodedParameters.time_hitbox_bomb_disappeared)
        		continue;
            if (bombe.getJustSpawned() == true){
                if (rectCircleColliding((Cercle)bombe.getHitboxBombe(), hitboxPlayer) == false)
                    bombe.notJustSpawned();
            }else{
                if (rectCircleColliding((Cercle)bombe.getHitboxBombe(), hitboxPlayer))
                   return false;
            }
        }
        return true;
    }

    @Override
    public void bindDataService(DataService service) {
        this.data = service;
        
    }   
    public static boolean rectRectColliding(Rectangle rect1, Rectangle rect2){
        if (rect1.top_left.x < rect2.top_left.x + rect2.width &&
        rect1.top_left.x + rect1.width > rect2.top_left.x &&
        rect1.top_left.y < rect2.top_left.y + rect2.height &&
        rect1.height + rect1.top_left.y > rect2.top_left.y) {
            return true; // collision
        }
        return false;
    }

    public static boolean circleCircleColliding(Cercle circle1, Cercle circle2){
        return circle1.center.distance_carre(circle2.center)
            <= (circle1.rayon+circle2.rayon)*(circle1.rayon+circle2.rayon);
    }
    
    public static boolean rectCircleColliding(Cercle circle, Rectangle rect){
        double distX = Math.abs(circle.center.x - rect.center.x);
        double distY = Math.abs(circle.center.y - rect.center.y);
    
        if (distX > (rect.width/2 + circle.rayon)) { return false; }
        if (distY > (rect.height/2 + circle.rayon)) { return false; }
    
        if (distX <= (rect.width/2)) { return true; } 
        if (distY <= (rect.height/2)) { return true; }
    
        double dx=distX-rect.width/2;
        double dy=distY-rect.height/2;
        return (dx*dx+dy*dy<=(circle.rayon*circle.rayon));
    }
}
