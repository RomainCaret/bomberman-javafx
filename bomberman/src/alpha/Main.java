/* ******************************************************
 * Simulator alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: alpha/Main.java 2015-03-09 buixuan.
 * ******************************************************/
package alpha;

import tools.HardCodedParameters;
import specifications.DataService;
import specifications.EngineService;
import specifications.ViewerService;
import specifications.AlgorithmService;
import specifications.CollideServices;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import algorithm.GameAction;
import collision.GestionCollide;
import data.Data;
import engine.Engine;
import gestionson.SonJeu;
import gestionson.Sound;
import userInterface.Viewer;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.stage.Stage;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.ImagePattern;
import javafx.util.Duration;

public class Main extends Application {
	// ---HARD-CODED-PARAMETERS---//
	private static String fileName = HardCodedParameters.defaultParamFileName;

	// ---VARIABLES---//
	private static DataService data;
	private static EngineService engine;
	private static ViewerService viewer;
	private static AlgorithmService algorithm;

	private SonJeu sj = new SonJeu(10, this);

	private static CollideServices collide;

	private Timeline timeline;
	private AnimationTimer timer;

	// ---EXECUTABLE---//
	public static void main(String[] args) {
		// readArguments(args);
		data = new Data();
		engine = new Engine();
		viewer = new Viewer();
		algorithm = new GameAction();
		collide = new GestionCollide();

		
		((Engine) engine).bindDataService(data);
		((Engine) engine).bindAlgorithmService(algorithm);
		((Engine) engine).bindGestCollide(collide);

		((Viewer) viewer).bindReadService(data);
		((Viewer) viewer).bindStartEngineService(engine);
		((GestionCollide) collide).bindDataService(data);

		((GameAction) algorithm).bindSimulatorService(engine);
		((GameAction) algorithm).bindDataService(data);
		;

		engine.init();
		viewer.init();
		

		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		
		try {
			sj.start_sound_boucle("src/sound/menu.wav",100, 0.6, false, Sound.MainSong);
			VBox root = FXMLLoader.load(new URL("file:src/application/pageAccueil.fxml"));	
			
			Scene scene = new Scene(root);
			scene.getStylesheets().add("file:src/application/application.css");
			primaryStage.setTitle("Bomberman");
			primaryStage.setScene(scene);
			primaryStage.setFullScreen(true);
			primaryStage.getHeight();
			primaryStage.getWidth();
			
			Image a = new Image("file:src/img/accueilV7.jpg");
			
			BackgroundImage bc = new BackgroundImage(a, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, false, true));
        	root.setBackground(new Background(bc));

			Button butt_play = (Button) root.lookup("#bplay");
			
			butt_play.prefHeightProperty().bind(primaryStage.heightProperty().divide(10));
			butt_play.prefWidthProperty().bind(primaryStage.widthProperty().divide(3));

			Button butt_help = (Button) root.lookup("#bhelp");
			butt_help.prefHeightProperty().bind(primaryStage.heightProperty().divide(10));
			butt_help.prefWidthProperty().bind(primaryStage.widthProperty().divide(3));
			
			butt_help.setOnAction(new EventHandler<ActionEvent>() {
	            @Override
	            public void handle(ActionEvent event) {
	            	VBox v;
					try {
						v = FXMLLoader.load(new URL("file:src/application/pageHelp.fxml"));
						BackgroundImage b = new BackgroundImage(new Image("file:src/img/Maquette.png"), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, false, true));
		            	v.setBackground(new Background(b));
		            	
		            	Button accueil = (Button) v.lookup("#baccueil");
		  
		            	accueil.prefHeightProperty().bind(primaryStage.heightProperty().divide(8));
		    			accueil.prefWidthProperty().bind(primaryStage.widthProperty().divide(3));
		            	
		            	accueil.setOnAction(new EventHandler<ActionEvent>() {

		    	            @Override
		    	            public void handle(ActionEvent event) {
		    	            	primaryStage.setScene(scene);
		    	            	primaryStage.setFullScreen(true);	    	        		
		    	        		primaryStage.show();
		    	        		sj.sound_dude(1);
		    	            	
		    	            }
		            	});
		 
		            	Scene scene_help = new Scene(v);
		            	
		            	primaryStage.setScene(scene_help);
		            	primaryStage.setFullScreen(true);
		        	
		        		primaryStage.show();
		            	
				
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
			}});
	            	
			
			
			butt_play.setOnAction(new EventHandler<ActionEvent>() {

	            @Override
	            public void handle(ActionEvent event) {
	            	viewer.startViewer();
	            	data.bind_bruit_boucle(sj);
	        		Scene scene = new Scene((Parent) ((Viewer) viewer).getPanel(0));
	        		
	        		Image image_bck = new Image("file:src/img/bcg2.png");
	        		primaryStage.setScene(scene);
	        		scene.setFill(new ImagePattern(image_bck));

	        		// stage.setWidth(HardCodedParameters.defaultWidth);
	        		// stage.setHeight(HardCodedParameters.defaultHeight);
	        		primaryStage.setFullScreen(true);
	        		
	        		
	        		
	        		
	        		primaryStage.show();

	        		timeline = new Timeline();
	        		timeline.setCycleCount(Timeline.INDEFINITE);
	        		timeline.setAutoReverse(true);
	        		
	        		/*Code pour bruits en boucle*/
	        		sj.start_sound_boucle("src/sound/zelda.wav",100, 0.1, false, Sound.MainSong); //String file, délai, skip premiere fois, mainSong
	        		/*Fin Code*/
	        		
	        		
	        		timer = new AnimationTimer() {
	        			@Override
	        			public void handle(long l) {
	        				scene.setRoot((Parent) ((Viewer) viewer).getPanel(l));
	        				// Pour les audio
	        				switch (data.getSoundEffect()) {
	        				case PhantomDestroyed:
	        					//new MediaPlayer(new Media(getHostServices().getDocumentBase() + "/sound/explosion1.mp3")).play();
	        					break;
	        				case Bombe:
	        					sj.sound_explosion(0.2);
	        					break;
	        				case HeroesGotHit:
	        					sj.sound_hit(0.5);
	        					break;
	        				case Birds:
	        					sj.sound_birds(0.5);
	        					break;
	        				case Dragon:
	        					sj.sound_dragon(0.7);
	        					break;
	        				case Door:
	        					sj.sound_door(0.7);
	        					break;
	        				case Death:
	        					sj.sound_death(0.1);
	        					break;
	        				case Item:
	        					sj.sound_item(0.1);
	        					break;
	        				case Enfants:
	        					sj.sound_enfants(1);
	        					break;
	        				default:
	        					break;
	        				}
	        				data.setSoundEffect(Sound.None);

	        			}
	        		};

	        		scene.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
	        			public void handle(KeyEvent ke) {
	        				switch (ke.getCode()) {
	        				case S:
	        					data.setGoDown(true);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				case Z:
	        					data.setGoUp(true);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				case D:
	        					data.setGoRight(true);
	        					data.setOrientation(NodeOrientation.LEFT_TO_RIGHT);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				case Q:
	        					data.setGoLeft(true);
	        					data.setOrientation(NodeOrientation.RIGHT_TO_LEFT);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				case DOWN:
	        					data.setGoDown(true);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				case UP:
	        					data.setGoUp(true);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				case RIGHT:
	        					data.setGoRight(true);
	        					data.setOrientation(NodeOrientation.LEFT_TO_RIGHT);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				case LEFT:
	        					data.setGoLeft(true);
	        					data.setOrientation(NodeOrientation.RIGHT_TO_LEFT);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        					
	        				case SPACE:
	        					data.addBomb();
	        					ke.consume();
	        					break;
	        				default:
	        					break;
	        				}
	        			}
	        		});
	        		scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
	        			public void handle(KeyEvent ke) {
	        				switch (ke.getCode()) {
	        				case S:
	        					data.setGoDown(false);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				case Z:
	        					data.setGoUp(false);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				case D:
	        					data.setGoRight(false);
	        					break;
	        				case Q:
	        					data.setGoLeft(false);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				case DOWN:
	        					data.setGoDown(false);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				case UP:
	        					data.setGoUp(false);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				case RIGHT:
	        					data.setGoRight(false);
	        					break;
	        				case LEFT:
	        					data.setGoLeft(false);
	        					ke.consume(); // <-- stops passing the event to next node
	        					break;
	        				default:
	        					break;
	        				}
	        			}
	        		});

	        		timeline.getKeyFrames().add(new KeyFrame(Duration.millis(HardCodedParameters.enginePaceMillisSound)));
	        		timeline.play();
	        		timer.start();
	            }
			});
			
			
			
			
			primaryStage.show();
			sj.sound_dude(1);
		} catch(Exception e) {
			e.printStackTrace();
		}

	}

	// ---ARGUMENTS---//
	private static void readArguments(String[] args) {
		if (args.length > 0 && args[0].charAt(0) != '-') {
			System.err.println("Syntax error: use option -h for help.");
			return;
		}
		for (int i = 0; i < args.length; i++) {
			if (args[i].charAt(0) == '-') {
				if (args[i + 1].charAt(0) == '-') {
					System.err.println("Option " + args[i] + " expects an argument but received none.");
					return;
				}
				switch (args[i]) {
				case "-inFile":
					fileName = args[i + 1];
					break;
				case "-h":
					System.out.println("Options:");
					System.out.println(
							" -inFile FILENAME: (UNUSED AT THE MOMENT) set file name for input parameters. Default name is"
									+ HardCodedParameters.defaultParamFileName + ".");
					break;
				default:
					System.err.println("Unknown option " + args[i] + ".");
					return;
				}
				i++;
			}
		}
	}
}
