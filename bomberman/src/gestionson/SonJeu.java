package gestionson;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javafx.application.Application;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;




public class SonJeu {
	private MediaPlayer[] tab;
	private int cpt_sound;
	private int main_song = -1;
	
	private MediaPlayer explode1;
	private MediaPlayer explode2;
	private boolean e1 = false;
	
	private MediaPlayer hit;
	
	private Application app;
	
	private MediaPlayer birds;
	private MediaPlayer dragon;
	private MediaPlayer door;
	private MediaPlayer item;
	private MediaPlayer death;
	private MediaPlayer dude;
	private MediaPlayer enfants;
	
	public SonJeu(int nb_sounds, Application app) {
		this.cpt_sound = 0;
		this.app = app;
		this.tab = new MediaPlayer[nb_sounds];
		
		this.explode1 = new MediaPlayer(new Media(app.getHostServices().getDocumentBase() + "src/sound/bomb.wav"));
		this.explode1.setOnReady(() -> {
        	this.explode1.setCycleCount(-1);
		});
		
		this.explode2 = new MediaPlayer(new Media(app.getHostServices().getDocumentBase() + "src/sound/bomb.wav"));
		this.explode2.setOnReady(() -> {
        	this.explode2.setCycleCount(-1);
		});
		
	
		this.hit = new MediaPlayer(new Media(app.getHostServices().getDocumentBase() + "src/sound/hit.wav"));
		this.hit.setCycleCount(-1);
		this.hit.setVolume(1);
		
		this.birds = new MediaPlayer(new Media(app.getHostServices().getDocumentBase() + "src/sound/mouettes.wav"));
		this.birds.setCycleCount(-1);
		this.birds.setVolume(1);
		
		this.enfants = new MediaPlayer(new Media(app.getHostServices().getDocumentBase() + "src/sound/enfants.wav"));
		this.enfants.setCycleCount(-1);
		this.enfants.setVolume(1);
		
		this.dragon = new MediaPlayer(new Media(app.getHostServices().getDocumentBase() + "src/sound/dragon.wav"));
		this.dragon.setCycleCount(-1);
		this.dragon.setVolume(1);
	
		this.door = new MediaPlayer(new Media(app.getHostServices().getDocumentBase() + "src/sound/door.wav"));
		this.door.setCycleCount(-1);
		this.door.setRate(1.45);
		this.door.setVolume(1);
		
		this.item = new MediaPlayer(new Media(app.getHostServices().getDocumentBase() + "src/sound/item.wav"));
		this.item.setCycleCount(-1);
		this.item.setVolume(0.1);

		this.death = new MediaPlayer(new Media(app.getHostServices().getDocumentBase() + "src/sound/mort.wav"));
		this.death.setRate(1.5);
		this.death.setVolume(0.1);
		this.death.setCycleCount(-1);
		
		this.dude = new MediaPlayer(new Media(app.getHostServices().getDocumentBase() + "src/sound/dude.wav"));
		this.dude.setCycleCount(-1);
		this.dude.setVolume(1);
	}
	
	public void sound_explosion(double volume) {
		
		if (e1) {
			this.explode2.setVolume(volume);
			this.explode2.setOnEndOfMedia(() -> {
	        	this.explode2.stop();
			});
			
			this.explode2.play(); 
			e1 = false;
		}else {
			this.explode1.setVolume(volume);
			this.explode1.setOnEndOfMedia(() -> {
	        	this.explode1.stop();
			});
			
			this.explode1.play(); 
			e1 = true;
		}
	}
	
	public void sound_hit(double volume) {
		this.hit.setVolume(volume);
		this.hit.setOnEndOfMedia(() -> {
			this.hit.stop();
		});
		this.hit.play();
		
		
	}
	
	public void start_sound_boucle(String file, int temps_attente, double volume, boolean skip, Sound type) {
		if (type == Sound.MainSong) {
			if (this.main_song != -1) {
				tab[main_song].pause();
			}	
			this.main_song = this.cpt_sound;
		}
		int loc_i = cpt_sound;
		this.cpt_sound++;
		Media media = new Media(app.getHostServices().getDocumentBase() + file);
		tab[loc_i] = new MediaPlayer(media);
		tab[loc_i].setCycleCount(1);
	
		tab[loc_i].setVolume(skip ? 0: volume );
		tab[loc_i].setOnReady(new Runnable() {

	        @Override
	        public void run() {
	            tab[loc_i].setOnEndOfMedia(() -> {
	            	tab[loc_i].stop();
	    			Timer t = new Timer();
	    			
	    			t.schedule(new TimerTask() {
	    				public void run() {
	    					synchronized (tab[loc_i]) {
	    						try {
	    							TimeUnit.MILLISECONDS.sleep(temps_attente);
	    							//temps d'attente
	    						} catch (InterruptedException e) {
	    							e.printStackTrace();
	    						}
	    						
	    					}
	    					tab[loc_i].setVolume(volume);
	    					tab[loc_i].play();
	    				}
	    			}, 0);
	    			
	    		});
	            tab[loc_i].play();
	        }
	    });
	}

	public void sound_birds(double volume) {
		this.birds.setVolume(volume);
		this.birds.setOnEndOfMedia(() -> {
			this.birds.stop();
		});
		this.birds.play();
		
	}

	public void sound_dragon(double volume) {
		this.dragon.setVolume(volume);
		this.dragon.setOnEndOfMedia(() -> {
			this.dragon.stop();
		});
		this.dragon.play();
		
	}
	
	public void sound_door(double volume) {
		this.door.setVolume(volume);
		this.door.setOnEndOfMedia(() -> {
			this.door.stop();
		});
		this.door.play();
		
	}
	public void sound_enfants(double volume) {
		this.enfants.setVolume(volume);
		this.enfants.setOnEndOfMedia(() -> {
			this.enfants.stop();
		});
		this.enfants.play();
		
	}
	
	public void sound_item(double volume) {
		this.item.setVolume(volume);
		this.item.setOnEndOfMedia(() -> {
			this.item.stop();
		});
		this.item.play();
		
	}
	
	public void sound_death(double volume) {
		this.death.setVolume(volume);
		this.death.setOnEndOfMedia(() -> {
			this.death.stop();
		});
		this.death.play();
		
	}
	
	public void sound_dude(double volume) {
		this.dude.setVolume(volume);
		this.dude.setOnEndOfMedia(() -> {
			this.dude.stop();
		});
		this.dude.play();
		
	}
}
